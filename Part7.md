# [7: Rules for Event Holders]{.new} # {#rules-for-event-holders}

## [7.1]{.new}: Becoming A Realms Event-Holder ## {#becoming-a-realms-event-holder}

To attempt to become an Event Holder you must first throw an event with your
name listed as an Event Holder.
To be a Realms legal event, the following conditions must apply:

* It should be open to the public.
* It needs to have been advertised with full event information at least two
  weeks before the date of the event on RealmsNet. If RealmsNet is unavailable,
  advertisement must include at least one of the following means instead:
  Publication in The *View from Valehaven* or *Creathorne Chronicles*, or sent
  through a mass mailing or electronic mailing, which must include the known
  addresses of all Realms EHs who have held Realms events in the previous year.
* The location and the price of the event must be announced at least two weeks
  before the event is scheduled to take place. Any significant rule changes,
  especially those affecting normally legal powers and abilities of PCs, must
  be announced in the advertisement.
* The names of the EHs, at most one per day, must be announced at least 5 days
  before the event is scheduled to take place.
* A site must be confirmed before an event can be advertised as a legal Realms
  event.
* The event must be at least six hours long.
* At least 30 people (including marshals and NPCs) must attend and the EH must
  be able to prove attendance through signatures.
* Players must have the option of playing their normal PCs, within such bounds
  and restrictions as previously advertised by the EH. Players who do so,
  despite any such advertised rule changes which may adversely affect their PC,
  are presumed to freely accept such changes or restrictions.
* The rules must be based either on the current Omnibus to the Realms or on
  another rule system approved at the most recent Event Holders' Council.
* If the EH(s) are under the age of 18, they must have an adult(s) either as a
  co-EH(s) or as a Listed Contact(s) in the event description. One of the said
  adults should be on-site for the event.
* An EH's PC may not benefit in any way from their event. Their PC may not take
  possession of any stealable item released at their event for more than one
  day. They may not receive spell credit as a result of attending their own
  event.
* In order for the event to be legal the event must abide by the valid rulings
  of the [Arbitration Committee](#arbitration-committee).

As an Event Holder you are expected to:

* Place the safety and welfare of players above all else.
* Show concern and caution towards sick and injured players.
* Be impartial, consistent, objective, and courteous when making decisions.
* Accept responsibility for your actions and decisions, as well as those of
  your staff.
* Respect the individuality of other players and event holders.
* Avoid any situations which may lead to or be construed as a conflict of
  interest.
* Always be respectful to other members of the community.
* Respect the land you are using, follow any site rule imposed by the land
  owner, and leave the site in better condition than you found it.
* Keep up to date with the latest rules of the game and principles of their
  application.
* Refrain from any form of personal abuse or harassment towards players or
  other Event Holders.
* Respect the rights, dignity, and worth of all people involved in the game
  regardless of their race, gender, sexual orientation, ability, or cultural
  background.

Throwing a legal event gives an EH certain privileges and responsibilities
above and beyond a normal player.

* To validate your event you must confirm it electronically with the official
  [Event List Administrator](#event-list-administer) within one month of the
  date of the event.
* [Also within one month, if any instances of rules violations were reported, you
  must send to the [Arbitration Committee](#arbitration-committee) a summary of
  those reports, conclusions of investigations into them, and consequences
  levied.]{.new} EHs and the Abitration Committee have the responsibility to
  maintain the privacy of those involved in reports to the extent possible.
* If you release a [Realms Magic Item](#magic-item-rules) at your event you must
  submit the name, physical description, and magical description to the [Keeper
  of the List of Realms Magic Items](#magic-item-marshal) within one month of
  that event. The actual prop for the item does not need to be distributed to
  players at that time, but an EH that does not designate a magic item for an
  event within one month forever loses the opportunity to [release an item for
  the event](#magic-item-rules).
* As an EH you qualify to attend [the Event Holders' Council meeting](#the-event-holders-council) for the
  year following your event.

## [7.2:]{.new} Magic Item Rules ## {#magic-item-rules}

[Each]{.new} EH is permitted to create one non-event specific magic item (a "[Realms Magic
Item](#magic-item-rules)") at each event for which they receive EH credit. Only
one magic item [per Event Holder]{.new} may be released for each event.
Such magic items will retain their powers and abilities at all subsequent events
at which they are pre-registered/checked in, and approved. Magic items are
released on the date of the event the EH is creating them for and must be
submitted to the [Keeper of the List of Realms Magic Items](#magic-item-marshal)
within one month of that event.

If an EH has not held a legal event during a calendar year, any items that they
back are still considered backed for an additional calendar year. This means if
an EH decided to take a calendar year off from throwing an event they can throw
an event during the next calendar year and their items will retain their
backing. However, if an EH does not hold a legal event during that following
calendar year, any magic items that they released will be considered
effectively destroyed as of the EH Council immediately following that year.

All magic items must have the following written on them or on an accompanying
tag: the item’s name; the responsible EH’s name; the date it was released;
the labels “Magic” and “Stealable”. [The [list of legal Magic 
Items](https://www.realmsnet.net/magic_items) is available on Realmsnet.]{.new}

## [7.3]{.new}: Challenging an Event ## {#challenging-an-event}

Questions on the validity of an event must be submitted to the organizer(s) of
the Event Holders' Council no later than one week prior to the Players'
Meeting. The organizer(s) are then responsible for requesting from the EH(s) a
list of players present at the event, and for attempting to contact at least
10 of them for a statement on if they felt the event met the legal
requirements or not, and why. This information must then be presented during
the administration section of the Event Holders' Council where all EHs present
will vote on the legality of the event.

If the EH Council finds that the event was not legal, any magic items issued
must be pulled, any ticks issued are removed, and the EH(s) who held the
event lose their Event Holder status for the year in question. The EHC may
impose additional sanctions, including suspension of Event Holding privileges,
as they see fit.

## [7.4: Emergency EH Meetings]{.new} ## {#emergency-eh-meetings}

Any serious issue(s) may be brought to the attention of the Event Holders'
List. If at least 50% of the EHs agree that a personal meeting is required to
discuss the issue(s), then an emergency EH meeting can be called. This meeting
must be attended by at least 50% of legal Event Holders. For this purpose, a
legal event holder has thrown a legal event this calendar year or was eligible
to vote at the most recent EHC. The person calling for the meeting will
work with the organizers of the upcoming Players' Meeting and Event Holders'
Council. Meetings of this nature will not have the ability to make changes to
the Omnibus.

An emergency EH meeting must be advertised with full event information at
least two weeks before the date of the intended meeting. Advertisement must be
on RealmsNet. If RealmsNet is unavailable, advertisement must include at
least one of the following:

* publication in The View from Valehaven or Creathorne Chronicles;
* sent through a mass mailing or electronic mailing, which must include the
  known addresses of all Realms EHs who've held events in the previous year.