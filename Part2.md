### [2.1: Participant Rights](./Omnibus.html#participant-rights) ###

If an EH subjects a player to a consequence at their event they __*should*__ notify
all other EHs of this via the Event Holders' List. The player involved must
receive a copy of this notification and may send a formal response. People who
want to bring formal charges should report such charges to the Arbitration
Committee (<arbitration@lists.realmsnet.net>). In addition, any participant that
feels wrongful action was taken against them can bring the issue before the
[Arbitration Committee](#arbitration-committee).

### [2.1: Participant Rights](./Omnibus.html#participant-rights) ###

If an EH subjects a player to a consequence at their event they should notify
all other EHs of this via the Event Holders' List. The player involved must
receive a copy of this notification and may send a formal response. People who
want to bring formal charges __*should*__ report such charges to the Arbitration
Committee (<arbitration@lists.realmsnet.net>). In addition, any participant that
feels wrongful action was taken against them can bring the issue before the
[Arbitration Committee](#arbitration-committee).

### [2.1: Participant Rights](./Omnibus.html#participant-rights) ###

Reports of rules violations to be submitted after the conclusion of an event
__*should*__ be submitted to the Arbitration Committee.

### [2.2: Code of Conduct](./Omnibus.html#code-of-conduct) ###

If you have reason to believe rules regarding the Code of Conduct may be
violated at an event, you __*should*__ notify the Event Holder prior to the event
occurring.

### [2.2: Code of Conduct](./Omnibus.html#code-of-conduct) ###

The following list is not meant to be exhaustive, and any non-consensual
violations of a participant's rights __*should*__ be considered a violation of the
rules.

### [2.2: Code of Conduct](./Omnibus.html#code-of-conduct) ###

Role-play can never be used as an excuse for any of these behaviors. If you are
informed that your role-playing is OOC unsafe, threatening, or is not consented
to by other participants you __*should*__ stop immediately and find another way to
play out the scene.

### [2.3: The Safety Rules](./Omnibus.html#the-safety-rules) ###

The safety rules are out-of-character (hereafter referred to as OOC). They are
for our safety, and provide the guidelines that we __*should*__ all be playing by.

### 2.3 § [The Rules We Play By](./Omnibus.html#the-rules-we-play-by) ###

1. We __*should*__ all be doing this to have fun. If you get mad or uncontrolled, it
   is up to you to remove yourself from the game until you have regained your
   composure.

### 2.3 § [The Rules We Play By](./Omnibus.html#the-rules-we-play-by) ###

5. HOLD: If you see a harmful or unsafe situation (e.g., someone is about to run
   into a tree, gets their glasses knocked off, has had their weapon really
   broken in combat, is about to fall off a cliff, etc.) yell the word "Hold."
   If someone is injured, it is the primary responsibility of the person who is
   hurt to call a Hold. Before calling a Hold for someone else in an otherwise
   safe situation, you must first ask if they are all right. Holds __*should*__ only
   be called in the event of a dangerous situation, and should never be used to
   discuss the rules. If you hear the word "Hold," stop immediately, then say
   "Hold" until everyone else has stopped moving. Once the emergency has been
   dealt with, a "Lay-On" (continue play) will be called either by a qualified
   marshal, or the person who originally called the Hold. Do not resume play
   until a Lay-On has been called.

### 2.3 § [The Rules We Play By](./Omnibus.html#the-rules-we-play-by) ###

5. HOLD: If you see a harmful or unsafe situation (e.g., someone is about to run
   into a tree, gets their glasses knocked off, has had their weapon really
   broken in combat, is about to fall off a cliff, etc.) yell the word "Hold."
   If someone is injured, it is the primary responsibility of the person who is
   hurt to call a Hold. Before calling a Hold for someone else in an otherwise
   safe situation, you must first ask if they are all right. Holds should only
   be called in the event of a dangerous situation, and __*should*__ never be used to
   discuss the rules. If you hear the word "Hold," stop immediately, then say
   "Hold" until everyone else has stopped moving. Once the emergency has been
   dealt with, a "Lay-On" (continue play) will be called either by a qualified
   marshal, or the person who originally called the Hold. Do not resume play
   until a Lay-On has been called.

### 2.3 § [The Rules We Play By](./Omnibus.html#the-rules-we-play-by) ###

6. Only weapons and shields that have been made following the [construction
   guidelines](#weapon-construction) are
   to be used. Weapons __*should*__ be checked between combat situations to ensure
   continued safety. A qualified marshal may be requested to check the safety of
   any weapons or shields at any time. Any new designs or materials must be
   inspected and approved by the EH or a designated marshal before use.

### 2.3 § [The Rules We Play By](./Omnibus.html#the-rules-we-play-by) ###

10. While the wording of the game rules may occasionally be less than clear,
    players __*should*__ not use any such confusion to their own advantage. A simple
    guideline is to not assume any benefits unless they are specifically granted
    by a rule. If you feel that the way a rule is written grants you an advantage
    by omission of a statement to the contrary, you must review that rule with a
    marshal or EH prior to utilizing that advantage. While this document has a
    lot of general rules (such as [Basic Magic Effects Everyone
    Should Know](#basic-magic-effects-everyone-should-know)), any specific rule
    will always override these general rules.

### 2.3 § [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

11. This is a lightest touch sport. ANY contact with a weapon to a body is to be
    taken as a hit. Ignoring a "light" blow is cheating and a marshal may remove
	you from the fight. There are to be NO full-strength swings. A marshal may
	remove you for excessive blow strength. Weapons, melee and missile, __*should*__
	be used with the minimum force necessary to score a successful hit.

### 2.3 § [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

15. Shields are for blocking ONLY. Your shield __*should*__ never be used as a weapon.
    Shield-bashing or other shield contact with another person is unsafe.

### 2.3 § [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

17. Do NOT ever throw a weapon at a participant, unless that weapon is of a type
    (magic missile, javelin, or lightning bolt) sanctioned by the rules for
    throwing. No thrown weapon or missile weapon __*should*__ ever be targeted at 
    your opponent's head or neck.

### 2.3 § [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

18. Arrows __*should*__ be drawn with minimal pull necessary to score a successful
    hit. Bows should NEVER be used to parry an attack. As with thrown weapons,
	arrows should not be targeted at your opponent's head or neck.

### 2.3 § [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

18. Arrows should be drawn with minimal pull necessary to score a successful
    hit. Bows __*should*__ NEVER be used to parry an attack. As with thrown weapons,
	arrows should not be targeted at your opponent's head or neck.

### 2.3 § [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

18. Arrows should be drawn with minimal pull necessary to score a successful
    hit. Bows should NEVER be used to parry an attack. As with thrown weapons,
	arrows __*should*__ not be targeted at your opponent's head or neck.