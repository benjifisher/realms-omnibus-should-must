# [8: Rules Changes and Administrative Meetings]{.new} #{#rules-changes-and-administrative-meetings}

## [8.1: Proposals]{.new} ## {#proposals}

[Any player may propose a rule change to be considered during the next series
of Administrative Meetings. These are the Players' Meeting and Event Holders'
Council. The purpose of these meetings is to vote on modifications to the rules
and administrative positions for the following year.]{.new}

[Proposals may bypass the Players' Meeting if they gain enough Event Holder
support. For this bypassing, at least 10 EHs legally able to attend and vote at
the Event Holders' Council must support the proposal. This support and the
proposal must be submitted to the organizer(s) of the Event Holders' Council at
least one week prior to the Players' Meeting.]{.new}

[Proposals may continue to be submitted online up to the day before the Players'
Meeting, and in person the day of the Players' Meeting. No new proposals will be
accepted after the Players' Meeting.]{.new}

[Proposals must meet certain requirements to be voted on at Administrative
Meetings.]{.new}

[A proposal must contain the following:]{.new}

* [The submitter's name and a way to contact them;]{.new}
* [A title and brief summary of the changes, including the intended impact of
  the proposal;]{.new}
* [The exact wording to be added, removed, or modified within the Omnibus, along
  with the section to be modified.]{.new}

[A proposals may also contain:]{.new}

* [Multiple options to be voted between if the proposal passes, such as a
  proposal seeking to clarify a rule in one of two ways;]{.new}
* [The name and contact information of a proxy player, to accept amendments to
  the proposal at the Players' Meeting.]{.new}

## [8.2]{.new}: Players' Meeting ## {#players-meeting}

Any player who has attended a legal Realms Event in the previous calendar year
can attend the Players' Meeting. The meeting is usually held several weeks
prior to the Event Holders' Council. This meeting is a forum for feedback on
rule proposals, rule changes, etc. If two-thirds of those voting on a proposal
support it, the proposal will be included as an official proposal at the Event
Holders' Council.

Those who attend the Players' Meeting shall be permitted to select one
representative (not an EH) to attend the Event Holders' Council. This
representative will have the same voting ability at the meeting as an EH, and
may accept amendments to player proposals submitted at the Players' Meeting.

## [8.3]{.new}: The Event-Holders' Council ## {#the-event-holders-council}

Any EH who has thrown an event between January 1 and December 31 (inclusive) of
the previous year may attend the annual Event Holders' Council.

Although any EH may attend the meeting, there are certain criteria that you
must meet in order to vote there. You must have hit at least six events,
including your own, in the same year that you held your event. Additionally
only one EH per day of the event will be eligible to vote. For the purposes
of this rule, a day runs from sunrise that calendar day to sunrise the next
calendar day, and an event day must be at least six hours long. For example,
an event starting at 9:00 pm on Friday and ending more than six hours after
sunrise on Sunday may have three EHs.

If an EH cannot or does not attend their own event (due to real life
emergencies, extenuating circumstances, etc.), they may still attend the
EH meeting, but their eligibility to vote and back magic items will be
reviewed by the remaining members of the Event Holders' Council to determine
their eligibility on a case-by-case basis.

No proxies will be accepted at the Event Holders' Council. Voting at the
Event Holders' Council will be by majority rule - a two-thirds majority of yes
over no, with abstentions not being counted, equals a majority. No guests
shall be permitted to attend the Event Holders' Council. The Moderator,
Co-Moderator, and Secretary will be considered staff and not guests. Expert
witnesses may be allowed if necessary, with an appropriate vote.

[The Players' Representative selected at the Players' Meeting will have 5 minutes 
before the first vote to introduce themselves to the council, to communicate 
perspectives that were particularly important to the players they represent, and 
to share any other information they or the players they represent consider 
relevant and significant to the council for the upcoming votes]{.new}

Each year at the Event Holders' Council several marshals are elected to help
streamline the administration involved with running the Realms. [You can find 
a brief description of each position and the names of those currently elected in
[Realms Administrative Positions](#realms-administrative-positions).]{.new}

At the end of the Event Holders' Council, the EHs will discuss and vote on
whether to and how to [allow additional grandfathering for the upcoming year]{.new} 
based on the changes made to the Omnibus[, such as allowing [spell]{.new}casters to update their 
Spell Mastery when spells change names or allowing players additional freedom in 
changing their spellbooks]{.new}.