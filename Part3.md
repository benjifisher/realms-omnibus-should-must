# 3: Combat in the Realms # {#combat-in-the-realms}

## 3.1: The Combat System ## {#the-combat-system}

The Realms uses a lightest-touch system for its combat system. That means that
no matter how lightly your opponent may touch you with their weapon, you have to
take the shot. This is to keep the sport friendly, so that everyone can play.
This system does not allow for "scratches" or "light blows" - you must take
these shots as normal blows. In all cases, the phrase, "if you are struck,"
refers to any time you are struck by the padded surface of any weapon or spell
prop that does damage as a weapon.

### Hit Locations ### {#hit-locations}

Your body is separated into seven locations of which there are two kinds. Your
arms and legs are "limbs," while your head and the front and back of your torso
are "kill locations." If you become confused about exactly where one location
stops and another begins, thinking of a Barbie™ doll may help. The buttocks of a
person are considered leg shots. If you are hit in the buttock(s), you lose the
appropriate leg(s).

### Killing Blows ### {#killing-blows}

If you are struck in a kill location your PC is [dead](#character-death).

If you are hit on the top of the shoulder, your PC is dead. If you are wearing
armor, a blow to the top of the shoulder is considered a blow to the front or
back depending on whether your opponent is in front of you or behind you.

### Limb Shots ### {#limb-shots}

If you are struck in a limb, your PC loses the use of that entire limb. If you
are struck in a limb that has already been lost, and that limb blocked what
could have possibly been a legal shot to another location, then that location
should be considered hit. You cannot protect the side of your PC's body with a
disabled arm or by lifting a disabled leg up to block. Once your PC's limb has
been disabled, it should be put behind you. This keeps it out of the way as well
as provides a visual cue to other players that your PC is hurt. Once you have
lost a limb, your PC cannot use that limb at all. Don't limp on a damaged leg.

### Hand-on-Weapon ### {#hand-on-weapon}

If you are struck on a hand that is holding anything that is legal to block or
parry with (weapons and shields, but not bows, javelins or arrows), it is
considered gauntleted and immune to damage. When this happens, call out "Hand"
or "Hand-on-weapon." If you are struck on a hand that is not holding anything
you can parry with, even if you have just taken it off for a second, it is
considered a legal limb shot and your PC has to suffer the consequences. Your
hand is considered everything below the wrist bone.

### How to Take Multiple Hits ### {#how-to-take-multiple-hits}

Should an opponent's weapon (be it a melee weapon, missile weapon, or spell
prop) hit you in more than one location with the same swing, all points of
contact count as hits.

## 3.2: Combat Etiquette ## {#combat-etiquette}

Combat is an important part of the game. In order to make the game more fun for
everyone, combat etiquette, which is OOC, has been developed. Using combat
etiquette helps avoid confusion and promotes the same kind of behavior when
someone else is fighting you.

### Calling Hits ### {#calling-hits}

It is your responsibility to call where you were hit in combat. This is to let
the other participant(s) and the marshals know that you were hit, and where you
were hit. Calling armor, protections, and other effects is also your
responsibility. Armor must always be called, even when a PC is dead. It is not
acceptable to call another player's hits for them. If you feel that another
participant is consistently miscalling their hits, missing shots, or some other
form of cheating, please alert a marshal. It is acceptable to ask your opponent
if they were hit, calling their attention to the location.

### Late Shots ### {#late-shots}

Sometimes you will strike another participant immediately after receiving a hit
that injures or kills your PC. For example, just before your sword makes contact
with an opponent, your attacking arm is struck by another weapon. Even though
you are incapable of physically halting your attack, the injury your PC received
renders the hit ineffective. There is no "follow through" effect that allows
your attack to be successful. This is called a late shot. If you deliver a late
shot, it is your responsibility to inform your opponent to not take the blow.
Common phrasing includes "Don't take that!" or "Late, on your arm!" Like other
shots, you may not declare that someone else hit you late. If you think someone
is failing to call their late shots, question them after combat ends, or bring
your concern to a marshal.

### Illegal Hits ### {#illegal-hits}

Sometimes you may hit someone with a part of your weapon that doesn't actually
damage their PC, such as the pipe of a weapon, the side of a [thrust-only weapon](#thrust-only-weapons),
or the shaft, fletching or
nock of an arrow or javelin. If you do this, be sure to call "Don't take that!"
to alert them that it wasn't a legal hit, and that they may resume play as if
the hit never occurred. A player may also call "shaft" if they received an
illegal hit from an arrow or javelin, or call "slash" if they received an
illegal hit from a thrust-only weapon.

### Missing Shots ### {#missing-shots}

Sometimes in the thick of battle people miss shots, because of adrenaline or
focus. This is generally unintentional and accidental. It often stops once a
fighter has more experience in calling and feeling their shots. Repeatedly
missing shots or intentionally missing shots is different and is cheating. If
you repeatedly miss shots, you may be asked to get retrained in fighting, or may
be asked to stop playing.

### Off-Target Areas ### {#off-target-areas}

Your face and the front of your throat are off-target; players should never aim
attacks there. If you are hit in either location, you should announce it even
though the hit has no game effect. Face is considered the area on your head
below your eyebrows, in front of your ears. Throat is considered the
forward-facing section of your neck, above the sternum. The forehead from the
eyebrows up, the back and top of your head, and the sides and back of your neck
are all legal targets.

Should an opponent's weapon hit you in more than one location with the same
swing and one or more of those blows land in an off-target area (such as your
face or your throat), you still have to accept those blows which did land in
legal locations.

Breast shots and groin shots are legal and considered killing shots, but such
shots are highly discouraged. Everyone is encouraged to wear protective gear to
avoid injury to sensitive areas.

You should never deliberately aim for an off-target area (face with any weapon,
or head with a ranged weapon) or the groin or breasts. Accidents happen, but if
you frequently target these sensitive areas it will indicate to others that you
are not a safe and controlled fighter, and you may be asked to sit out by a
marshal.

## 3.3: Armor ## {#armor-section}

### Types of Armor ### {#types-of-armor}

Your PC may be able to wear armor. Armor allows a PC to take blows without
taking injuries. There are two kinds of armor: heavy and light. Heavy armor will
absorb two blows per hit location before you have to take the shot. Light armor
will absorb one blow per hit location.

Light armor is a thin, pliable kind of armor. Light armor can be made out of
multiple layers of quilting or a moderate weight of leather (approximately 2-4
oz). A quilted cloth jerkin (such as a piece that is thin, and does not easily
return to its proper shape after bunching or a thin leather helmet are examples
of 1-point armor. Garment-weight leather (such as suede pants or moccasins) is
not suitable for light armor.

Heavy armor is in general bulkier, rigid, and more cumbersome than light armor.
Hard-boiled leather; heavy-weight leather (approximately 5+ oz); studded
leather/cloth; cloth that has obvious thickness to it, is made from multiple
layers of material, and when bunched and released, quickly returns to its proper
shape; brigandine; and chainmail/platemail are classic examples of 2-point
armor. Any armor that is studded must have at least one metallic component every
square inch to count as heavy armor.

Armor must allow you to feel blows through it and it must look like it would fit
in a medieval or fantasy setting.

### Calling Your Armor ### {#calling-your-armor}

Armor protects by hit location, so if you have more than one piece of armor on a
hit location, it is all considered damaged when you are struck there. On the
other hand, if one piece of armor covers more
than one hit location, it is treated as separate hit locations. The armor hit
locations are divided up by the hit locations for taking wounds and kills. Light
armor allows you to call out "Armor 1" on that hit location and the next blow
will do damage. If you are wearing heavy armor you should call out "Armor 1"
then "Armor 2" before the hit location takes damage.

You can be wearing armor on a hit location yet still have some areas of that hit
location that are not covered. These sections are also considered armored if at
least 75% of the entire hit location is covered by armor. If some of that armor
is light and some is heavy then the uncovered areas can only call out "Armor 1".
If all of the armor on the hit location is heavy then then the uncovered areas
can call both points of armor. A marshal should be asked to rule whether or not
75% of the location is covered in any case where it is ambiguous.

For example, if you are wearing a thin leather upper-arm bracer and a metal
lower-arm bracer on your right arm, only a leather upper-arm bracer on your left
arm, and a chainmail shirt that goes down to your knees: a shot to anywhere on
your lower-left arm disables your PC's left arm. The upper bracer was not struck
and thus has no benefit. A shot to the upper portion of your left arm damages
the upper-arm bracer. Your PC still has the limb, but the armor on that limb is
gone. A shot to the lower-right arm bracer damages all the arm armor. You still
have a point of armor left on the lower bracer, but if you are hit on the
upper-right arm bracer, your PC will lose the limb. Had the next shot also hit
the right lower-arm bracer, all the right arm armor would be gone. Three shots
to a leg covered by the chainmail skirt would destroy all of the armor on that
leg and disable the PC's leg as well. Even though the armor protecting the leg
and the armor protecting the torso is all one piece, you can still take two
shots to the armor on your front torso, back torso, and your other leg before
the armor in those locations is completely destroyed.

## 3.4 Character Death ## {#character-death}

As players experience the game through the eyes of their characters, PC death
becomes an important aspect of the game.

### Death ### {#death}

Death occurs in many ways. Usually, death of a character happens when something,
such as a weapon, hits a character in an unarmored kill location. Certain spells
may also kill a character, from magic missiles to ingesting poisons or other
more esoteric means. Death renders the character incapable of any action until
such time as a spellcaster or item, with the power to heal the dead, raises the
character, or magic that regenerates or animates them takes effect.

Death can be repaired by many spells, abilities, and items. Be sure to go over
[Combat in the Realms](#combat-in-the-realms) and [Basic
Magic Effects Everyone Should Know](#basic-magic-effects-everyone-should-know)
for more details on what causes and cures death.

In the Realms there are two states of death. They are death (or dead), and
[soulless](#soul-loss).

If your PC is dead or soulless, you should lie or sit still. Try not to look
around or talk. Do your best to role-play a corpse. Don't get upset if someone
hits you with a killing blow when you are already dead. If somebody does this
just say, "Dead." They are making sure that you really are dead.

In tournaments, or other high combat situations, it is acceptable for a
character to move out of the way to avoid being stepped on. They may resume
their death act in a safer place. They may also sit or kneel to avoid injuries.
If you are role-playing death in any of these forms, you should put your weapon
over your head to signify your character is dead.

If someone looks at you and asks you to describe your wounds, do your best to
comply. If you were backstabbed and you're lying on your back, tell them they
don't see any wounds. Then, if they roll you over to look at your back, tell
them they see a deep wound in your back.

When your PC is killed (dead, but not soulless) and then raised, they may
remember everything up until the point of their death.

### Playing Dead ### {#playing-dead}

It is legal for characters to lie down and pretend they are dead, but they may
not put their weapons over their head. If someone asks a player if their
character is dead, the player and the character are not obligated to answer, but
if a player is asked to describe their character's wounds, they should do so as
accurately and honestly as possible. If their character is not really dead and
someone comes close to them to loot their character's body, they are free to
attack the unsuspecting looters. If you are unsure as to whether someone's
character is dead and want the character to be, tap the player gently in a kill
location.

## 3.5: Soul Loss ## {#soul-loss}

Soul loss is the removal of a character's life essence. It is a more serious
form of death that requires more than a simple spell to repair. A character who
has lost their soul cannot be raised or animated until their soul has been
restored.

### Soul Tokens ### {#soul-tokens}

A soul token is a token carried by each player which represents the life essence
of that player's character. Players must be carrying their PC's soul token at
all times, unless the PC is dead and soulless,
or unless they are under the influence of a
spell which removes the PC's soul. A soul token must have its player's name and
the PC's name written legibly on it. A soul token does not exist physically in
the IC world; therefore it cannot be searched. A soul token is a non-magical and
non-stealable marshaling tool.

### Destroying a Body ### {#destroying-a-body}

In order for a player to separate a soul from a body, they must destroy the
body. This is done by by striking 200 blows with a weapon beside the body being
destroyed. More than one person may destroy a body at a time. More than one
weapon may be used to destroy a body as well. This effectively divides the
number of blows to be struck between the number of participants and number of
weapons used. Some monsters and characters under certain spells may require more
blows to completely destroy. If you strike 200 blows and the victim says "The
job is not yet done," then the body is not yet destroyed. Some spells, such as
[Strange Brew: Potion of Acid](#potion-of-acid) and [Assassin's
Blade](#assassins-blade), may accelerate the act of destroying a body.

After the body is destroyed, the character whose body has been destroyed is
rendered soulless. The player of the soulless character must present their
character's soul token to the character that just destroyed their body. It is
then that player's responsibility to present the soul token to an EH or an
appointed marshal. The EH must be informed immediately of the body's
destruction, thus allowing the EH time to prepare for those who might wish to
return the soulless character to life.

Characters that are pretending to be dead should interpret any body destroying
blows as blows to the closest kill location. So, if your PC is pretending to be
dead and someone starts to destroy their body, the first blows that are struck
on the ground next to you should be played as if they were striking your PC on
the nearest kill location, destroying any undamaged armor, and then killing
them.

If your PC is killed and their body is destroyed, they will not remember
anything about how they died should they manage to be raised.

Should your PC be dead at the end of an event without being raised, even if the
character's body was not actively destroyed, the PC will be considered soulless.

### Fixing a Soulless Character ### {#fixing-a-soulless-character}

To restore someone to life after they have lost their soul, characters must
first have the body of the person needing to be raised. Then characters must
either cast a [Call the Soul](#call-the-soul), an [Intervention](#intervention)
spell, or administer a [Potion of Soul Snare](#potion-of-soul-snare)
to summon and reattach the soul. If characters
lack the body, only an [Intervention](#intervention) spell will be able to
return the soulless PC back to life. When the body and soul are reunited,
characters must cast one [Raise Dead](#raise-dead) spell for each event
(including the first) since the soulless PC was rendered soulless in order to
raise them. A different spellcaster must provide each [Raise Dead](#raise-dead)
spell used for this purpose.

## 3.6: Permanent Death ## {#permanent-death}

When a PC is dead and soulless at the end of an event at which they were at some
point alive, they get a "tick." PCs may also receive ticks due to use of certain
magic items or plot interactions. A PC is only obligated to accept one
involuntary tick per event. A PC that accumulates three or more ticks is
permanently dead, as their soul can no longer be restored by any means. On
January 1st of each new year, one tick is removed from each PC that has any,
unless they are already permanently dead.

If a PC is killed and rendered soulless during an event, but returned to life
before the end of the event, they do not get a tick.

For a body's complete destruction to be official, it must be brought to the EH's
attention. The EH must provide the Death Marshal with this information from
their events. A tick may only be issued or reported by a legal EH of the event
where the tick was incurred or by the player whose PC received the tick. The
[Death Marshal](#death-marshal) will keep track of this information.

## 3.7: Weapon Rules ## {#weapon-rules}

Players are responsible for being safe with the weapon(s) they are using. Before
using a weapon style in-game, players should take it upon themselves to be
properly trained by a marshal or someone who is safe and proficient with that
weapon style.

### Wielding Weapons ### {#wielding-weapons}

For the purposes of the rules, you are wielding a weapon or shield if you are
holding it in your hand(s) and you attack, parry, or block with it. You are
wielding a combination of weapons and/or a shield if you attack, parry, or block
with either of them. Simply holding a weapon, or menacing with it, does not
count as wielding unless contact is made. You cannot wield more than one item in
a hand at once.

If you are wielding an illegal weapon or combination and you attack, then you
should tell the target "Don't take that." If something you are not wielding
blocks a shot, then you should treat the blow as if it had landed. If you are
not sure where that blow would have landed, then assume it would hit the
location that would cause you the most harm (e.g., an unarmored kill location).
There are special restrictions for spellcasters in terms of wielding and
blocking with different weapons, and specific consequences for doing so outside
of the spellcaster's restriction. See [Breaking Weapon
Restriction](#breaking-weapon-restriction) for more information.

The size of a weapon dictates how it may be wielded. Weapons cannot be any
smaller than 12", and cannot exceed 8' in length.

+-------------+-------------------------+
| Length      | Weapon Type             |
+============:+:========================+
| 12" to 3'8" | One-handed              |
+-------------+-------------------------+
| 3'8" to 5'  | Hand-and-a-half         |
+-------------+-------------------------+
| 4' to 5'    | One-handed, no          |
|             | florentine, thrust-only |
+-------------+-------------------------+
| 5' to 6'6"  | Two-handed              |
+-------------+-------------------------+
| 6'6" to 8'  | Two-handed, thrust-only |
+-------------+-------------------------+

Table: Weapon Length Quick Guide

### One-Handed Weapons ### {#one-handed-weapons}

You can wield a one-handed weapon or shield in one hand and still use another
one-handed weapon or shield in the other hand. Using two one-handed weapons (up
to 3'8" each) together, one in each hand, is commonly called a "Florentine"
combination. [Magic Missiles](#magic-missile) are also considered one-handed weapons, but follow
special rules.

### Hand-and-a-Half Weapons ### {#hand-and-a-half-weapons}

If a weapon is considered hand-and-a-half, then you can use the weapon with one
hand, but your other arm or hand cannot be holding a weapon or a shield.
Lightning bolts are also considered hand-and-a-half weapons. [Bows](#bows) are wielded as
hand-and-a-half weapons, but follow special rules.

### Two-Handed Weapons ### {#two-handed-weapons}

If a weapon is two-handed, you may only wield the weapon with two hands. Every
blow you strike must be started with two hands. If you lose an arm while
wielding a two-handed weapon, you may not attack with that weapon. You may parry
with a two-handed weapon with only one hand, but only if you are not wielding
something else with your other hand.

Every blow you strike with a two-handed weapon must begin with both hands on the
weapon. If you let go with one hand during the swing, the swing is still legal.
Once the initial swing has ended, the attacker will have to grab the weapon with
two hands again before making another attack.

### Thrust-Only Weapons ### {#thrust-only-weapons}

Thrust-only weapons cannot strike an opponent with a "slashing" or side-to-side
motion. If you slash at an opponent with a thrust-only weapon, you must tell
them "Don't take that." As long as the tip of the weapon strikes with a forward
motion the blow must be taken. If you are not certain that a thrust-only strike
landed properly, you must assume it was a slash and tell your opponent not to
take the shot.

### Spears (Thrust-only) ### {#spears}

A spear is a thrust-only weapon between 4' and 5' in length. These weapons may
be used as a single-handed weapon if used alone or with a shield. Spears may not
be used with other weapons. Their length must be between 1/3 to 1/2 covered in
foam and they must be clearly labeled with the word "Spear" on the blade. Once
being labeled as a spear the weapon must stay thrust-only even if it is legally
made to regular slashing hand-and-a-half standards.

### Bows ### {#bows}

A bow is wielded as if it were a hand-and-a-half weapon. This means that it may
not be held when a weapon or shield is being wielded in your other hand, but it
is legal to fire it with one hand --- if you can! A bow is not considered a
"weapon" for purposes of determining whether a spell fails; arrows are
considered weapons and will cause some spells to fail.

Bows must have a full draw weight of 30 pounds or less. Just like melee weapons,
you should be careful on how hard your arrows are striking your opponent. Arrows
should be drawn with the minimal pull necessary to score a successful hit.

If a weapon hits a wielded bow or nocked arrow, the bow is "broken" and may no
longer be used in combat. Anyone can fix a broken bow by holding the bow with
both hands, then counting to [120]{.new} seconds. You cannot actively parry with a bow
or an arrow.

### Javelins ### {#javelins}

A javelin is wielded as if it were a single-handed weapon. This means that it
may be held when a weapon or shield is being wielded in your other hand. A
javelin is considered a weapon for the purposes of spells.

### Missile Weapons ### {#missile-weapons}

Projectile weapons[,]{.new} such as arrows, [javelins, and spells with missile props 
(like [Magic Missile](#magic-missile) and [Lightning Bolt](#lightning-bolt-spell)),]{.new}
cannot be targeted at your opponent's
head. Head [and neck]{.new} shots from projectile weapons, even if they did not hit the face or
the throat, do not have to be taken as legal shots.

Being struck with any portion of the foam head of the arrow or javelin will
cause damage. The shaft, fletching, and nocks of arrows and javelins do not
cause damage.

All projectile weapons, with the exception of arrows and javelins, are live and
inflict damage on any target they hit until they come to rest, regardless of
whether they hit the ground, a wall, a weapon, a tree, or any other obstacle
along the way. [An arrow is only considered live when it is shot from a bow. 
A javelin is only considered live when it is thrown. Arrows and javelins are 
able to inflict damage until they come into contact with the ground.]{.new}

It is the responsibility of the player wielding the bow or javelin to tell you 
not to take the blow. A player may also call "shaft" if they received an illegal 
hit from an arrow or javelin. The length of javelins and arrows are not subject 
to a spellcaster's weapon restriction.

## 3.8: Weapons Construction ## {#weapon-construction}

There are several ways to make weapons in the Realms. If you are playing for the
first time, it might be a better idea to borrow weapons than try to make any of
your own. Once you see what other weapons look like and ask people about how
they made their weapons, you will be better prepared to construct your own.

Be sure to follow these guidelines when constructing a weapon:

* All non-missile melee weapons must be made out of lightwall PVC, fiberglass,
  or bamboo cores as noted in the table below.
* There can be nothing in the core, and both ends must be capped with a rigid
  material.
* All weapons must have a thrusting tip (the pipe foam that extends beyond the
  tip of the pipe) as noted below.
* A weapon's striking surface must be made from closed-celled foam pipe
  insulation, which must be at least 5/8" thick and which must be firmly
  strapping-taped in place.
* All striking surfaces must be covered in either duct tape or fabric.
* Only magic weapons should be made with a blue-colored striking surface, and
  you may only make a magic weapon through the use of spells in the magic
  system, or if you are releasing one as an EH.

+--------------------+-----------------+-----------+-----------+-----------+---------------+
| Length             | Core            | Thrusting | Camp      | Squishy   | Minimum Foam  |
|                    |                 | Tip       | Foam Disk | Foam Tips | Coverage      |
+:==================:+:===============:+:=========:+:=========:+:=========:+:=============:+
| 12" - 44"          | 1/2", 3/4",     | 1 3/4"    | Not       | Not       | Half of       |
|                    | or 1" PVC       |           | Required  | Required  | weapon length |
+--------------------+-----------------+-----------+-----------+-----------+---------------+
| 12" - 44"          | [3/8"]{.new}    | 1 3/4"    | 1/4"      | Not       | Half of       |
|                    | bamboo or       |           |           | Required  | weapon length |
|                    | [1/2"]{.new}    |           |           |           |               |
|                    | fiberglass      |           |           |           |               |
+--------------------+-----------------+-----------+-----------+-----------+---------------+
| \>44" - 60"        | 3/4" or 1"      | 1 3/4"    | Not       | Not       | Half of       |
|                    | PVC             |           | Required  | Required  | weapon length |
+--------------------+-----------------+-----------+-----------+-----------+---------------+
| \>44" - 60"        | [1/2"]{.new}    | 1 3/4"    | 1/4"      | Not       | Half of       |
|                    | bamboo or       |           |           | Required  | weapon length |
|                    | [3/4"]{.new}    |           |           |           |               |
|                    | fiberglass      |           |           |           |               |
+--------------------+-----------------+-----------+-----------+-----------+---------------+
| Spears\            | 3/4" or 1"      | 2 1/4"    | 1/4"      | 2"        | One-third     |
| (48" - 60")        | PVC             |           |           |           | weapon length |
+--------------------+-----------------+-----------+-----------+-----------+---------------+
| Spears\            | 1/2" bamboo     | 2.5" *    | 1/4"      | 3"        | One-third     |
| (48" - 60")        |                 |           |           |           | weapon length |
+--------------------+-----------------+-----------+-----------+-----------+---------------+
| \>60" - 78" \      | 3/4" or 1"      | 2 1/4"    | Not       |[2+"]{.new}| Half of       |
| (6'6")             | PVC             |           | Required  |           | weapon length |
+--------------------+-----------------+-----------+-----------+-----------+---------------+
| \>60" - 78" \      | 3/4" bamboo     | 2 1/4"    | 1/4"      |[2+"]{.new}| Half of       |
| (6'6")             | or fiberglass   |           |           |           | weapon length |
+--------------------+-----------------+-----------+-----------+-----------+---------------+
| Pikes\             | 1" PVC          | 2 1/4"    | 1/4"      | 2"        | One-third     |
| (>78" - 96")       |                 |           |           |           | weapon length |
+--------------------+-----------------+-----------+-----------+-----------+---------------+
| Pikes\             | [3/4"]{.new}    | 3" *      | 1/4"      | 3"        | One-third     |
| (>78" - 96")       | bamboo          |           |           |           | weapon length |
+--------------------+-----------------+-----------+-----------+-----------+---------------+

\* add 5" camp/yoga foam sleeve starting beneath the foam disk extending away
from the tip

### Double-bladed weapons (including Marns) ### {#double-bladed-weapons}

* No surface of a double-bladed one-handed slashing weapon may be shorter than
  6".
* The overall (combined) foam length must still be at least half the length of
  the weapon.
* Each striking surface of a double-bladed slashing weapon of hand-and-a-half
  length or longer must be at least 1/3 the total weapon length.
* Weapons with a non-bladed portion longer than the shorter of the two blades
  must provide non-damaging covering foam or other padding as a courtesy to
  shield the exposed core.

### Arrows, Javelins, and other Missile Weapons ### {#arrows-javelins-and-other-missile-weapons}

__T8, golf tube, and Aqua Tube Arrows__

* All arrows must have 2" thrusting tips, 4" of pipe foam, and 2" squishy-foam
  tips to be considered safe.
* All arrows must be made from 3/4" or 1" diameter tubing.
* All arrow tube shafts must be capped on the arrowhead end of the shaft with a
  rigid material. Examples of acceptable cap materials include, but are not
  limited to, rigid plastics (such as milk jug caps or film canister caps) or a
  single washer (non-metal washer) or coin. Any such rigid materials must be
  appropriately sized to match the arrow shaft in diameter.
* Arrows may not be weighted in any way beyond the materials necessary for safe
  construction. Weighting includes the attachment of multiple rigid caps.
* All aqua tube shafts should, at minimum, be covered by two lengths of
  strapping tape.
* Lightning Bolt props are the only "arrow" props that should be made solely
  from white duct tape. Arrows may have a white head or shaft, but not both.
* Whether custom-designed arrows are allowed is up to the individual EH's
  discretion.

__LARP Arrows__

* Head must be permanently affixed, commercially manufactured, designed to be
  "larp safe" or "archery tag safe" with squishy foam, and have a minimum
  diameter of 2 1/8" on the striking surface.
* Shaft must be made from either unbreakable or safety fiberglass, covered in a
  minimum of 2 layers of fiberglass reinforcing tape (strapping tape), and be no
  longer than 28" measured from the end of the nock to the base of the arrow
  head.
* Nocks must be permanently affixed and must be manufactured and designed to be
  "larp safe" or "archery tag safe".

[__Lightning Bolts__]{#lightning-bolt-construction}

A Lightning Bolt prop is a T8, golf tube, or aqua tube arrow, or a javelin, made
with the following additional details:

* It must be primarily white.
* It must be between 2'6" and 3'6" in length. The prop is not subject to the
  spellcaster's weapon length restriction.
* It must have the words "Lightning Bolt" and the spellcaster's name written
  visibly along the shaft.
* It cannot have an arrow nock.
* It must not have the word "Javelin" on it if it is a javelin prop.

__Javelins__

* Javelins may be constructed using golf tubes or aqua tubes with foam coverage
  on at least 1/3 the weapon length.
* Javelins cannot be nocked, and must have foam coverage on the non-damaging end
  of the shaft.
* Javelins must be made from new pipe foam and must have squishy-foam heads.
* Any fletching added must be made out of foam, and the javelin may not be
  weighted in any way.
* Javelins must be between 2'6" and 5' in length.
* The word "Javelin" must also be written on the side of the weapon.

## 3.9: Shield Construction ## {#shield-construction}

* Shields must be at least 12" long (as measured by the longest dimension). A
  shield may be as large as you like, but you as an individual must be able to
  safely wield it. A marshal or EH can pull a shield if they feel it is unsafe.
* Shields can be made of any safe material, such as wood, plastic, or cardboard.
  Metal shields are heavy, but are allowed if otherwise safe.
* All shields must have their edges covered by foam. Any protruding metal screws
  or bolts should also be padded.

## 3.10: Equipment Inspections ## {#equipment-inspections}

You must inspect any armor, weapon, or shield before using it. If you are unsure
about an item's safety, ask a marshal and they will inspect it for you. Any item
can be inspected at any time during an event at anyone's request. This is meant
for the purpose of ensuring safety and should never be used for strategic or
tactical purposes. The EH or a designated marshal retains final ruling on the
approval for use of any armor, weapon, or shield.

### Armor Inspection ### {#armor-inspection}

Armor can be failed or the point value of armor may be adjusted for many
reasons. Armor can have exposed edges that could cause a safety concern for the
wearer or other combatants. The EH or designated marshal retains final ruling on
the point value and approval for use of armor.

### Shield Inspection ### {#shield-inspection}

Shields can be failed for many reasons. A shield can have exposed edges or
protrusions that could cause a safety concern for the wearer or other
combatants. A shield can be failed if it has seen too much abuse and has not
been repaired recently. The EH or designated marshal retains final ruling on the
approval for use of a shield.

### Weapon Inspection ### {#weapon-inspection-construction}

Weapons can be failed for many reasons. A weapon can have too much "whip" (one
that flexes too much) or not enough "whip" (one that doesn't flex at all). There
is no standard way of measuring flex, you will have to use your common sense. A
weapon can be failed if it has seen too much abuse and has not been repaired
recently. The most common problem weapons have is that their thrusting tips are
breaking down or have been compacted. The EH or marshal retains final ruling on
the approval for use of a weapon.

## 3.11: Combat Calls ## {#combat-calls}

Combat calls are what you may hear yelled in combat, and you must know how these
calls affect your PC.

### Armor ### {#armor-call}

Negates a hit attack. When "Armor" (or "Armor 1," "Armor 2," or "Armored Cloak")
is called in combat, it means that the person calling armor is protected against
the attack that landed on them, usually by means of armor or a spell effect.

### Armor-Piercing ### {#armor-piercing}

Armor cannot always protect a PC from certain attacks. If an opponent attacks a
PC in any way and calls out "Armor-Piercing," any of the armor that is struck by
the attack is completely destroyed, and the PC suffers the effect of the blow as
if they were not wearing armor. For example, if a PC is wearing heavy armor on
their right arm, and an opponent hits it while calling "Armor-Piercing," the
PC's armor is destroyed on that arm and they lose the limb.

### Boulder ### {#boulder}

A PC can encounter a boulder call in one of two ways: being hit with a physical
boulder prop or by a weapon simulating the effects of a boulder. In either
instance, the same call of "Boulder!" is used.

If you or any of your equipment are hit by the call of "Boulder!", you suffer
the following effects: your character is dead, any armor hit is "broken" where
the boulder touched the locations, magic items hit by the boulder are
disenchanted, and any non-monetary equipment (i.e. non-enchanted weapons, bows,
and shields) that is hit by the boulder is "broken." It is the player's
responsibility to see that items damaged this way are not used until the
appropriate repair spells are cast upon them.

A boulder prop is often represented by throwing large duct-taped chunks of foam
or beanbag chairs with the zippers duct-taped over. When thrown, the boulder is
active until it comes completely to rest. PCs may not throw or pick up boulders.
Four or more PCs, using a total of eight hands on the boulder, may work together
to "push" a boulder along, to free trapped gear and companions, but not to cause
damage.

### Disease ### {#disease}

If a weapon strikes a PC and the wielder calls "Disease," wounds on their body
take twice as many spells to be healed until the disease is cured. This effect
only damages a PC, not their armor. For example, if you are hit by a diseased
blow to the leg, [Heal Limb](#heal-limb) would need to be cast twice on the leg
for it to be healed until the disease is cured. Similarly, if you are killed by
a disease shot, the spell [Raise Dead](#raise-dead) will need to be cast twice
on you until you are cured of the disease.

Regeneration effects will still affect a diseased character with a single use,
but the regeneration time is doubled until the disease is cured. Disease can
affect both living and dead characters.

[The spells used to heal a wound of a diseased character must be cast in succession 
(i.e., without other spells being cast on the character between the two spells).]{.new}

### Fireball ### {#fireball}

If you or any of your equipment are hit by the call of fireball, your character
is dead. Armor and weapons hit by the fireball are not broken, but also do not
protect against its damage.

The fireball call may come in the form of a thrown prop (as with a magic
missile) or as a weapon blow. Fireball is magical in nature.

### Flat ### {#flat}

To "knock out" an opponent, the attacking player must call out the word "Flat"
prior to their attack. Should a successful killing blow be struck, they have
instead rendered their foe unconscious for a steady count of [120]{.new} seconds. The
unconscious PC may be wakened sooner by having another PC come and wake them up
by touching the unconscious PC and saying "Wake Up." The PC can also wake up
when any damage is inflicted on them. You may call "Flat" at any time, even in
the middle of a fight. This form of attack deals no damage to armor.

### Impale ### {#impale}

Impaling is the act of holding a weapon in an opponent's kill location after
death, and saying the word "Impale." It counts as continuous,
non-body-destroying blows to that location. The act of impaling will bypass any
armor, damaged or not. While impaled, a character cannot be raised or animated
and is unable to regenerate. The results can vary when impaling NPCs.

### Lightning Bolt ### {#lightning-bolt-call}

If you are hit by any part of a white boff arrow and the thrower calls
"Lightning Bolt," your PC is struck as if by a [magic](#magic-call) [armor-piercing](#armor-piercing) weapon.

### Poison ### {#poison}

There may be occasions where a PC is struck with a blow and the wielder calls
"Poison." If this strike damages a PC in any location, be it torso, arm, leg,
etc., then the PC is killed. When the blow does no damage to a PC, such as a hit
to an off-target area or a hit to armor that protects the PC, then the poison
has no effect. Armor struck with a poison blow is still used. If a PC is under
the effects of a spell that protects them from poison in some fashion, the PC
still takes the normal damage from the blow but the poison will have no
additional effect. Players should also call "Immunity to Poison," to allow an
opponent to understand that you recognized that the blow was poisoned.

### Weapon Type and Material Calls ### {#weapon-type-and-material-calls}

Occasionally, more powerful monsters are only affected by certain weapon types
or by certain materials. For example, axe-mace trolls are typically only injured
by axes and maces; werewolves are only affected by weapons made of silver. It
might be a good habit when using a melee weapon other than a sword, to call the
type of weapon you are using as you swing. If using a mace, say "Mace" with each
swing. When you are wielding a non-normal weapon (e.g., magic or silver), that
weapon will always strike with the same effect therefore you must call that
effect with every swing.

### Magic ### {#magic-call}

If a weapon strikes you and the wielder calls "Magic" (or "Magic Missile"), it
means your PC has been hit with a magical blow. Generally, being hit by magic
doesn't affect your PC any differently than being hit by a normal weapon, but
sometimes PCs are under spell effects where it makes a difference.

### Silver ### {#silver}
If a weapon strikes you and the wielder calls "Silver" it means your PC has been
hit with a silver blow. Being hit by silver doesn't affect your PC any
differently than being hit by a normal weapon.

## [3.12: Non-Combatant Players]{.new} ## {#non-combatant-players}

[At any time during an event, a player may declare themself to be a
non-combatant. They do this by informing an EH or designated marshal and by
putting on a player-provided yellow armband on each arm (or on their head, if
their arms would otherwise be covered). Only players who declare themselves to
be non-combatants may wear a yellow armband. Once a player declares themself to
be a non-combatant, they will stay a non-combatant until the end of an event
(unless given approval by an EH). A player declaring themself to be a
non-combatant takes on a few unique rights and responsibilities:]{.new}

* [First, and most importantly, a non-combatant should never be hit by a weapon,
  and intentionally doing so is cheating and should be reported to a marshal.
  Instead, to kill a non-combatant, a player may simply point at them and say
  "die", provided either the player is wielding a melee weapon and is within 10
  feet of them, or the player is wielding a missile weapon. This can not be
  resisted by any means. If a non-combatant is regenerating, you can simply
  point at them and say "impale" and, while within 10 feet of the body, they
  will be considered impaled. They may be rendered soulless as normal.]{.new}
* [Second, a non-combatant may never wield a weapon and must take reasonable
  precautions to avoid looking like they are wielding a weapon. They may carry
  weapons and shields for others, but shall do so in such a manner that makes it
  clear that they are not being wielded. In addition, non-combatant [spell]{.new}casters may
  not use the spells [Magic Missile](#magic-missile) or [Lightning
  Bolt](#lightning-bolt-spell).]{.new}
* [Third, a non-combatant shall do their best to stay out of the flow of combat.
  Should a non-combatant die, and combat is near to them, they may walk while
  dead to an OOC-safer place for them.]{.new}
* [Fourth, a non-combatant must make sure their yellow armband is visible at all
  times. If it is not, they must remove themselves from any combat situations
  until they have a visible armband.]{.new}
* [Finally, a non-combatant is encouraged, but not required, to tell the EH of an
  event the reason for them to be a non-combatant, if the non-combatant feels
  that the information should be known by a member of staff.]{.new} 

[A yellow armband must meet the following requirements:]{.new}

* [It must be a dayglo yellow or other similar florecent yellow color with the
  words "Non-Com" on it.]{.new}
* [It must be at least 2 inches wide or actively glowing. At night it still must
  be visible, including being lit up, if needed to make sure it is seen.]{.new}
* [If a non-combatant's arms are covered, a headband matching these criteria may
  be worn.]{.new}