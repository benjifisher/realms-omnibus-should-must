# 6: Magic in the Realms # {#magic-in-the-realms}

## 6.1: Basic Magic Effects Everyone Should Know ## {#basic-magic-effects-everyone-should-know}

Even if you are not a spellcaster, and have no desire to become one, there is
still some basic information that you are responsible for understanding. In
nearly all cases, a spellcaster casting a spell on your PC should explain,
either through their Verbal Component or as an aside to you, how the spell
affects your PC. Even fighters should read the magic system and spell
descriptions, so they have a basic understanding of magic in the game and how it
might affect them. No spell effect may be ignored unless otherwise specified in
the spell system.

The following are some rules about spells that can have an effect on your
character on a day-to-day basis. They are included here with some basic
information about how they affect you both IC and OOC.

### Healing Spells ### {#healing-spells}

When your character is injured or killed, only magic can fix the damage. To
raise a character means to return a character to life. Raising a character
repairs any injuries to limbs the character may have taken prior to their death
and also nullifies the effects of any poisons in their system at the time of
their death.

* [Combat Raise Dead](#combat-raise-dead): this three-word spell will raise a
  character.
* [Cry of Life](#cry-of-life): the call of "All in the sound of my voice, rise
  and fight" raises all dead who hear it.
* [Group Healing](#group-healing): allows a spellcaster to make a large circle
  through which they cast a healing spell upon every character in it.
* [Heal Limb](#heal-limb): this spell allows one wounded limb to be magically
  healed. The spellcaster will let you know when your character's limb has been
  repaired.
* [Raise Dead](#raise-dead): this spell raises a character. This spell will not
  work if there is a weapon within 10 feet of the spellcaster. Inform a
  spellcaster the spell has failed if you know about a weapon near enough to
  disrupt the spell. A bow is not considered a weapon, but arrows are considered
  weapons.

### Regeneration ### {#regeneration-bmeesk}

Some spells grant the ability to regenerate from death. This takes 120 seconds.
While this happens, your wounds begin to heal. Until 120 seconds have passed,
this grants no benefit. A blow to any kill location on a dead body will cause a
regeneration count to reset no matter where the killing blow was inflicted.
[Impaling](#impale) stops regeneration; the count resets when the weapon is
removed. Regenerating from death heals all healable wounds on the body. If
examined by another person, wounds can be seen to be regenerating. If you are
[diseased](#disease), it takes twice as long to regenerate. The effect ends when
you are raised.

### Combat Spells ### {#combat-spells}

Certain combat calls ([Lightning Bolt](#lightning-bolt-call) and [Magic
Missile](#magic-call))
involve a prop that is thrown at a combatant. After the prop has come to rest it
is only a physical representation of magic and cannot be moved or touched other
than by the person who threw it. It may be seen and guarded but not purposefully
hidden (e.g., putting a bucket over it). The prop is not considered a weapon and
does not cause [Spell Failure](#spell-failure) except while the spell is active
(i.e. from when the prop is thrown until it comes to rest).

### Undeath ### {#undeath}

Certain spells make your character undead for their duration. Your character
will remember what happened to them while they were undead. While undead, your
character cannot cross a [Circle of Protection](#circle-of-protection), other
than one they created themselves (represented by a circle of rope on the
ground), or advance within 5 feet of someone casting [Ward:
Undead](#ward-undead) or [Ward: Enchanted Beings](#ward-enchanted-beings). This
family of spells does not give you the ability to ignore weapon restrictions,
nor can you be compelled to forcibly break them. If you have any questions about
this, ask the spellcaster when they cast the spell on you.

* [Animate Undead General](#animate-undead-general): if this spell is cast upon
  you when your PC is dead, your PC becomes an undead creature under the
  spellcaster's control. You may not refuse to have your PC turned into an
  undead, unless under [certain magical effects](#protect-the-soul).
  You are now playing a greater undead version of
  your PC and have access to all their spells, abilities, and knowledge. In
  addition your PC now has the ability to cast [Animate Lesser
  Undead](#animate-lesser-undead) with unlimited castings. The Undead General
  will die to any damaging magical attacks regardless of location. Your PC must
  follow the orders of the spellcaster. The spell ends if your PC is slain and
  raised or simply raised.
* [Animate Undead](#animate-undead): if this spell is cast upon you when your PC
  is dead, your PC becomes an undead creature under the spellcaster's control.
  You may not refuse to have your PC turned into an undead, unless under [certain
  magical effects](#protect-the-soul). You are now
  playing an undead version of your PC and have access to all their spells,
  abilities, and knowledge. Your PC must follow the orders of the spellcaster.
  The spell ends if your PC is slain and raised or simply raised.
* [Animate Lesser Undead](#animate-lesser-undead): if this spell is cast upon
  you when your character is dead, your character becomes an undead creature
  under the spellcaster's control. Lesser undead cannot use any armor or spells
  regardless of what they normally have. You may not refuse to have your PC
  turned into an undead, unless under [certain magical effects](#protect-the-soul).
  [Animate Lesser Undead](#animate-lesser-undead) can
  be used to raise an [undead](#undead), but
  returns the character to un-life, rather than life. Like [Animate
  Undead](#animate-undead), the spell ends when your PC is slain and raised or
  simply raised.
* [Walking Dead](#walking-dead): certain spells will make a PC's dead body walk
  without either returning them to life or fully animating them as undead. When
  a PC is under the effects of these spells, follow these guidelines:
  * Walk at a steady pace, do not run.
  * Keep any items on you that you would retain if picked up and dragged.
  * Move to the destination as directly as possible without taking an OOC unsafe
    path. If the only option is to move into an OOC unsafe situation, then the
    spell ends and the body falls to the ground.
  * If another character interferes with the body, such as by attacking them or
    physically stepping in their way to block them, the effect of the spell
    ends.
  * The PC cannot take any actions other than walking; no attacking, searching,
    picking up items, using magic items, or drinking potions.
  * The PC is considered to be Undead while the effect lasts. See the [Undead
    Caveat](#undead).
  * The main difference between various [Walking Dead](#walking-dead) spells is
    in where the PC must walk to.
    * [Beckon Corpse](#beckon-corpse): the PC will stand and walk to the
      spellcaster as long as they are chanting. If the PC is forced to stop, the
      spellcaster may have the option to regain their attention and resume
      chanting, at which point the PC will get back up and continue walking to
      the spellcaster.
    * [Zombie Walk](#zombie-walk): the PC will follow the spellcaster until the
      spellcaster either ends the spell, the spellcaster attacks someone, or
      they are attacked.

### Potions ### {#potions-bmeesk}

Potions are disposable magic items that anyone can use. Common forms that
potions can take include, but are not limited to something that must be consumed
or a scroll that must be read or ripped. In all cases the potion needs to be
administered by a living or animated character, and after it is used, cannot be
used again.

* [Potion of Acid](#potion-of-acid): if your PC is dead, and someone indicates
  they are using an acid potion on you, your PC takes 200 body
  destroying blows. See [Strange Brew](#strange-brew) for details.
* [Potion of Combat Raise Dead](#potion-of-combat-raise-dead): it raises a dead
  character when used.
* [Potion of Heal Limb](#potion-of-heal-limb): it heals all of a character's
  injured limbs when used.
* [Potion of Repair Armor](#potion-of-repair-armor): it repairs a hit location
  of damaged armor in 15 seconds when applied to it.

### Blacksmith Spells ### {#blacksmith-spells}

Certain spells repair damaged armor and broken items.

* [Repair Armor](#repair-armor): this will restore one hit location of
  non-magical armor that has been damaged. For example, the armor on one arm,
  the chest of a shirt of chainmail, head armor, etc.
* [Repair Item](#repair-item): this will restore non-magical armor, weapons, and
  other items that have become damaged or destroyed. This spell has a Verbal
  Component, and it fixes the entire item. For example, all hit locations
  of someone's armor, a bow, a boulder-crushed weapon, a shield, etc.

### Other Spells and Things You Should Know ### {#other-spells-and-things-you-should-know}

* [Create Poison](#create-poison): if you ingest food or drink that is poisoned
  through the use of this spell, you will be handed a scroll upon which is
  written the poison's effect. You must follow the scroll's instructions
  completely. It will either kill your PC, cause them to [view someone as 
  their best friend]{.new}, sleep deeply, or tell nothing but the truth. All 
  effects other than death are short-lived. A player that ingests a [charm]{.new} 
  poison always has the option of allowing their PC to die if they are OOC uncomfortable 
  with the situation.
* [Cure Disease](#cure-disease) or [Potion of Cure
  Disease](#potion-of-cure-disease): these spells cure a character of a [disease](#disease).
* [Immunity to Poison](#immunity-to-poison): this spell makes your PC immune to
  the very next poison or [poison attack](#poison) that would otherwise affect them. It
  works only once per use of the spell. Call "Immunity to Poison," when you use
  this spell's effect.
* [Light](#light): you may not take the light out of verbal communication range
  of the spellcaster.
* [Pas](#pas): this spell creates a temporary truce. If you accept the offered
  bribe you are magically bound to not attack the spellcaster for 60 seconds
  unless you are attacked.
* [Protect the Soul](#protect-the-soul): this spell will protect your PC from
  possession, [Animate Undead](#animate-undead), and the like.
* [Speak With Dead](#speak-with-dead): this spell will allow the spellcaster to
  ask a dead character a single question per casting. Your PC MUST answer
  truthfully or abstain, using the words, "Yes," "No," or "Abstain."

## 6.2: Learning and Unlearning Spells ## {#learning-and-unlearning-spells}

### Choosing Spells ### {#choosing-spells}

The first step in choosing spells is to decide which 1st circle spell you want
to learn. Each path of magic is a list of spells of 5 increasing circles.

A spellcaster may choose one of four options for each circle of a path:

* Any of the spells listed for that circle
* Any spell of a lower circle
* A [Regional Magic spell](#regional-magic)
* [Alchemy](#alchemy)

When a spellcaster learns their first path of magic, they gain spell slots for
each of 1st through 5th circles, in order. If they learn a second path, they
gain spell slots for each of 2nd through 6th circles, in order. If they learn a
third path, they again gain spell slots for each of 1st through 5th circles. The
same spell may be taken multiple times.

### Learning Progression ### {#learning-progression}

At every event a character attends, they have the possibility of learning one or
more spells. If they do not learn a spell at that event, for whatever reason,
the opportunity is wasted and they may try again at the next event.

### Learning a Spell ### {#learning-a-spell}

There are three ways to learn a spell. You should find a PC who knows the spell
and learn it from them. The PC who is teaching the spell must have the spell in
their spellbook, must be able to cast the spell at that event, and they must
sign the book with their character's name. If you cannot, or decide that you do
not want to learn it from a character who knows it, you can ask the EH to
provide you with a quest to learn a single spell. This quest will add a single
signature toward learning the spell. Additionally, you may teach yourself a
single spell if it is listed in your Spell Mastery section. Your PC does not
officially learn the spell until your teacher, teachers, or MM sign your
spellbook legibly. Your teacher or teachers are responsible for making sure you
understand all of the rules that go with the spell, and may refuse to sign if
you seem unable or unwilling to understand the rules. This is important, as the
teacher may be held liable for their student if they did not teach them the
spell properly.

You may learn as many spells as you are able to prove understanding for;
however, at any event, for each additional spell after the first an additional
teacher's signature is required. Your first spell requires one teacher's
signature, your second spell requires two teachers' signatures, your third spell
requires three teachers' signatures, etc. You must actually attend that event
and play that PC. Teaching yourself from Spell Mastery counts as a single
teacher's signature. A teacher or quest giver may only sign off on one spell per
player per event.

### Unlearning Spells ### {#unlearning-spells}

Any number of spells may be unlearned at check-in of an event. You may not learn
a spell at an event where you are unlearning any spells. If you unlearn all the
spells in a path, then you no longer have that path, and your restriction
immediately changes to match your current number of paths. When you have learned
2 paths, you may not unlearn your first path unless you are also unlearning the
second path completely. Likewise when you have learned 3 paths you may not
unlearn both paths of magic that include circles 1-5; you may only unlearn one
of those paths unless you are also unlearning all of your path that includes
circles 2-6. Upon unlearning all your spells, you are no longer considered a
spellcaster. Any given spell may not change the path it is in without being
unlearned from its original path and relearned in the new path.

For example, Ethan is a 2 path with some healing magic in his second path. He
decides that he doesn't like healing very much, and he misses using a 4'6"
weapon. At the next event he attends, he unlearns all of the spells in his
second path, and he begins the event at Light restriction.

Fiona is a 3 path, but wants to change her 6th circle spell. The next event she
attends, she unlearns her 6th circle spell, remaining under a Severe restriction
but without access to a 6th circle spell for the event. At the next event she
can learn a new 6th circle spell.

Gunthar wants to become a fighter after being an spellcaster for many years, but
he doesn't want to give up the option of learning spells again later that year.
At the beginning of the next event he attends, he unlearns all of his spells and
starts as a fighter.

## 6.3: The Basics of a Spell ## {#the-basics-of-a-spell}

### Spell Components ### {#spell-components}

Spells have components that are necessary in order to cast the spells. Some are
specific, and every player must use the same component to make sure that
everyone understands what spell your PC is casting. Some are left open and the
spellcaster can choose any component that fits the description. The spell
descriptions list the minimal spell components required for each spell. The game
does not limit the spellcaster's freedom to define their own magic, so the
required components are as succinct as possible. You may add more requirements
for shtick if you like, but you cannot leave out any of the minimums. [A spellcaster may
cast any number of spells at a time, provided at most one has a verbal component.]{.new}

Here are the definitions of the different types of components:

* Verbal Component (VC): These are the words you have to say while casting the
  spell. It is important that you enunciate your verbal component and say it
  loud enough so the person or persons affected can understand what you are
  saying. The verbal usually explains what spell you are casting. If the target
  cannot understand you, they are not affected by the spell. [Any required verbal
  must be recited without stopping or errors, with the exception of OOC explanations
  (such as combat calls).]{.new} A verbal must be
  written within the spellbook in order to be used, and it must meet the
  criteria for the spell. Requirements such as "Talk to the EH" or "an
  explanation" can simply be written as such. Multiple verbals may be written
  for the same spell, and the [spell]{.new}caster may choose which to use at casting time.
  Verbals may be changed between events.
* Material Component (MC): There are three types of material components;
  required, disposable, and foci.
  * Required components are specific to a spell, such as bean-bags, foam, or
    duct tape blocks for the [Magic Missile](#magic-missile) spell. These
    components cannot vary from what is listed.
  * Disposable components are up to the player, but they must be something that
    is consumed or thrown away with every casting of the spell. A disposable
    component is something that the spellcaster could easily hand to the MM for
    inspection.
  * A focus is a component that is not consumed or thrown away. Often, it is
    necessary for the spellcaster to brandish a focus while casting certain
    spells. The spellcaster may have a single focus for all of their focus-based
    spells. A focus is something that another player or NPC can obviously
    identify as the focus when the spellcaster is using it for a spell. A
    spellcaster must also be able to hand this to the MM for inspection. A focus
    may not be a weapon.

  All MCs must be specified and written down in the PC's spellbook for every
  spell that they know and, except for foci, the component must be different for
  each spell. A spellcaster must have at least one uninjured hand to use a MC.
* Active Component (AC): These are actions that the spellcaster must take in
  order to cast the spell and must be performed at the time of casting. [If the
  active component(s) of a spell are not performed throughout the entire casting
  process, the spell is not cast.]{.new} Characters may add anything else for 
  role-playing purposes.
* Duration: Unless otherwise noted in the spell description or caveat, all
  spells end when the event ends.

## 6.4: Alternatives to Spells ## {#alternatives-to-spells}

### Regional Magic ### {#regional-magic}

At any given event, the EH may wish or require that certain magical abilities be
available to the players. One of the ways they can accomplish this is through
Regional Magic. Regional Magic is usually an additional number of spells that
spellcasters can choose from. Spellcasters can only choose from this list if
they had filled at least one spell slot with a Regional Magic spell. Regional
Magic is learned and unlearned just like any other spell, and may be learned
from anyone who knows it at any circle.

At some events, the Regional Magic your PC will receive is based on which circle
spell slot you filled with the Regional Magic spell. At some events, all of the
Regional Magic spells are the same, no matter which circle slot you filled with
Regional Magic. Others are completely random. Some EHs may require you perform
certain actions before gaining the Regional Magic. The details of Regional Magic
are left entirely up to the EH. No Regional Magic spell will have a lingering
effect that lasts longer than the end of the event. One thing to keep in mind is
that while Regional Magic is more versatile, it is also more unreliable. An EH
may choose a different spell from the list, a new spell, or nothing.

### Alchemy ### {#alchemy}

A spellcaster may choose to take Alchemy in a spell slot instead of learning a
spell. This still requires someone to teach and sign off their spellbook. Any
spellcaster that knows Alchemy must include a points total on their spell list
page. Whenever a spellcaster learns Alchemy they receive points equal the chart
listed below. For example, if Samuel learns Alchemy in his third and fifth
circle slot, he will have 12 points with which to make potions. All spellcasters
know three basic potions, listed below, and can learn more with the Strange Brew
spell. All Potions are governed by the Enchanted Item and Potions Caveat.

+-----------------+----+----+----+----+----+----+
| Spell Circle    | 1  | 2  | 3  | 4  | 5  | 6  |
+-----------------+----+----+----+----+----+----+
| Points Gained   | 1  | 2  | 4  | 6  | 8  | 10 |
+-----------------+----+----+----+----+----+----+

Basic Potions

* (1 Point) [Potion of Repair Armor]{#potion-of-repair-armor}: Repairs one hit
  location of armor. The potion is poured or applied to the damaged armor and
  held there for a 15-second count. The armor does not need to be removed for
  the potion to be applied to it.
* (2 Points) [Potion of Mending]{#potion-of-mending}: Recipient receives a basic
  regeneration. This potion will have no effect on a living target.
* (Special) [Power Potion]{#power-potion}: Resets a single spell of the
  recipient equal to half of the points spent to create it, rounded down. This
  cannot be used to reset a 6th circle spell. (Example: a [spell]{.new}caster may spend 10
  points to make a potion to reset a 5th circle spell.) Each level of Power
  Potion requires either its own unique sigil or to be clearly marked with the
  spell circle it was created to reset.

## 6.5: Caveats ## {#caveats}

Caveats are general rules that apply to all spells or spell effects of a similar
type. Each spell that is affected by a caveat is listed in the appropriate
place.

### [OOC Calls]{.new} ### {#ooc-calls}

[These are special OOC calls to inform and describe specific IC game effects, 
These calls do not break verbals, chants or other VCs as they are purely game 
mechanics and martialing tools. All spells that utilize special combat calls 
describe the specific call that must be made with its use.]{.new}

### Chanting ### {#chanting}

Some spells require that their VC be chanted continuously for the duration of
the spell. These spells do not take effect until the VC has been recited fully,
at least once. These spells last as long as the spellcaster continues the chant.
OOC explanations (such as combat calls) do not interrupt these spells. For
example, if a PC is chanting a [Transmute Self](#transmute-self) spell and is
hit by a weapon, they may call "No effect" without interrupting the spell; if a
PC is chanting a [Ward: Undead](#ward-undead) spell and a goblin hits their leg,
they can call "Leg" (or "Armor," or "Armored Cloak," etc.) without having the
spell end. The VC for these spells must be spoken clearly and loudly enough that
anyone affected by the spell can understand them. Chanting spells can be
disrupted by the 4th circle spell [Disrupt](#disrupt). It is the PC's
responsibility to know what the [Disrupt](#disrupt) spell is, how to recognize
it, and how to respond to it.

### Circles ### {#circles}

There are a number of spells that are considered circle spells. A circle spell
must be clearly defined by a length of rope that has been laid on the ground
with the ends overlapping. The ends cannot be tied together or secured in any
way, and the rope in general cannot be secured or bound in place or the [spell
fails](#spell-failure). Only one spell may be
cast with a particular rope at a given time, although after the spell ends, a
different spell may be cast with that rope. The rope does not need to be laid
down in a circular pattern. Although a given circle spell may have a specific
way of being broken, all circle spells are broken if the rope is jostled enough
to move the ends apart by a character able to cross it. Any circle spell can be
suspended by the 4th circle spell [Disrupt](#disrupt). It is the PC's
responsibility to know what the [Disrupt](#disrupt) spell is, how to recognize
it, and how to respond to it.

### Compulsions ### {#compulsions}

Some spells are able to magically compel a character to act in a way the player
would prefer they did not. Compulsions can be ignored if they are humiliating or
exceedingly difficult commands such as "Kiss my feet" or "Move that wall ten
feet to the left." They also cannot violate OOC mundane laws or ethical codes.
They may not force a spellcaster to break their weapon restrictions.
Additionally, if the character has been turned undead, they cannot be compelled
to communicate knowledge gained before they were made undead.

### Enchanted Beings ### {#enchanted-beings}

All spellcasters, undead, and certain creatures are considered to be enchanted
beings. Normal fighters are only enchanted beings if under the effect of certain
spells, as per the [Undead Caveat](#undead). Enchanted beings are
affected by a certain number of spells, while non-enchanted beings are not.
These spells include [Circle of Protection](#circle-of-protection) and [Ward:
Enchanted Beings](#ward-enchanted-beings). By definition, any creature
considered undead is an enchanted being.

### Enchanted Items ### {#enchanted-items}

Some spells create or enchant items, or are enhancements that affect players.
These spells create magical effects, but are not potent enough for the items
bearer or the target of the enchantment to be considered an Enchanted Being, nor
is their magic potent enough for the item to be affected by the spell [Circle of
Protection](#circle-of-protection) once they are cast. If the MC of these spells
are disenchanted (through the spells [Disenchant](#disenchant) or the [Strange
Brew: Potion of Disenchant](#potion-of-disenchant) option) the spell will end,
rendering the MC inert. Magic items are not covered by the Enchanted Items
Caveat.

### Potions ### {#potions-caveat}

A spellcaster who learns [Alchemy](#alchemy) must have a page in their spellbook 
listing the sigils
that they will use to label potions. Each type of potion they can make, 
must have a unique, distinguishable sigil. When a potion is made, the 
spellcaster must put their legible signature, the
appropriate sigil, and the date upon the container. Once created, all potions
are considered stealable items. A potion can be a represented by a liquid,
lotion, elixir, magical food, or anything else, as long as it is safe to be
administered in a combat situation. Potions must be directly applied to the
recipient. They may not be thrown, dropped, or remotely applied in any manner.
The spellcaster need not be present in order to use their potions.

Potions take a 60 second process called Brewing to be cast. At the beginning of
an event, the spellcaster must describe in general terms their process of
brewing. An alchemist may brew as many potions as their spells allow during the
same 60 seconds. If they wish to create more later, they must begin the process
again. If the alchemist is interrupted while brewing, Alchemy points are [not]{.new} 
lost, but the potions will not be made, and they must start
over again in order to make any potions. Although not required, use of
additional props or role-playing is encouraged for this process.

No potion created by a player can carry over from one event to another; it
expires at the end of the event at which it is cast. The PC may choose whether
or not to further limit the lifespan of a potion when it is brewed by writing a
distinct expiration time among the required spell information on the container.
Any potions lacking a specified expiration time last until the end of the event.

### Regeneration ### {#regeneration-caveat}

Some spells grant the ability to regenerate. When this ability is triggered (by
death, being wounded, etc.), the target's wound(s) begin to heal. Until the
specified amount of time has passed, this grants no benefit. A blow to any kill
location on a dead body will cause a regeneration count to reset no matter where
the killing blow was inflicted. Impaling stops regeneration; the count resets
when the weapon is removed. Regenerating from death heals all healable wounds on
the body. If examined by another person, wounds can be seen to be regenerating.
Regeneration that brings a character back from death or soul loss takes 120
seconds. If you are [diseased](#disease), it takes twice as long to regenerate.
You may only be under the effects of one basic regeneration and one advanced
regeneration at a time. If more than one source is causing you to regenerate,
you may choose which of those spells is causing you to regenerate. A
regeneration will only work on someone who is soulless if the spell directly
states that it can.

### Basic Regeneration ### {#basic-regeneration}

A basic regeneration is a regeneration from death where the regeneration begins
when the target dies (or upon casting the spell if the target is already dead.)
The effect is considered used if the target is raised before their regeneration
is complete.

### Advanced Regeneration ### {#advanced-regeneration}

An advanced regeneration is a regeneration from death (or soul loss, if
specified in the spell description) which begins when the character dies (or is
rendered soulless, if it brings a character back from losing their soul). Each
time they die, the spellcaster may choose to double the length of their
regeneration (to 240 seconds, or 480 if diseased). The regeneration is not
considered used if the target is raised before their regeneration is complete.

### Spell Sash ### {#spell-sash}

Some spells require a Spell Sash be worn to signify that a specific spell is
affecting the wearer. A Spell Sash may be constructed as a sash, a tabard, or a
belt favor. A Spell Sash must have the name of the spell clearly written on it.
Putting the Spell Sash on the target of the spell, or touching it if the target
is already wearing it, is considered to be an Active Component of the spell. A
Spell Sash is not stealable, and should stay on the target of the spell until
the spell is ended, at which time it is removed and returned to the 
[spell]{.new}caster at the earliest convenient time. Either the [spell]{.new}caster 
or the target may choose to end the spell by removing the Spell Sash, and it is 
also ended early if the Spell Sash is [disenchanted](#disenchant).
The Spell Sash must be worn in such a way that another player standing 5 feet in 
front of them can recognise it, and may not be covered unless the majority of their 
body is covered, such as by wearing a concealing cloak.

### Spell Failure ### {#spell-failure}

Some spells have fail conditions in their description. Any spell that fails due
to its fail condition expends the casting of that spell.

### Suspension ### {#suspension}

While a spell is suspended, the spell has no effect and does not function until
the suspension ends. In addition, a suspended spell can be ended any way it
normally could. It is the spellcaster's responsibility to notify anyone else who
is affected by this (such as any players carrying potions that the spellcaster
created). When the suspension ends, any spell which has not already ended will
resume functioning as per normal.

### Undead ### {#undead}

Some spells will make a recipient undead. When the spell is cast, the
spellcaster must explain to the target what it means to be undead. All undead
are affected by the spells: [Circle of Protection](#circle-of-protection),
[Ward: Enchanted Beings](#ward-enchanted-beings), and [Ward:
Undead](#ward-undead). A PC will remember what happened to them while they were
undead. While undead the PC should have the spellcaster's best intentions in
mind. A PC may be raised as undead if they are [diseased](#disease), but this
does not cure them of the disease.

### Walking Dead ### {#walking-dead}

The spells [Beckon Corpse](#beckon-corpse) and [Zombie Walk](#zombie-walk)
enchant a corpse to walk without either returning them to life or being fully
animated as undead. A corpse under these effects must move at a walking pace
with their hands above their heads to their destination as dictated by the
spell.

They should keep any items on them that they would retain if dragged, whether or
not the items are stealable. If anyone directly interferes with their movement
or attacks them, they fall to the ground and the effect ends. The Walking Dead
cannot take any actions other than walking. They may not attack, search other
bodies, cast spells, pick up objects, use magic items, drink potions, or perform
any other action aside from movement. The effect does not stop regeneration or
other methods of returning to life or becoming fully animated from occurring.
Becoming alive or fully animated as undead ends the Walking Dead effect. Corpses
under these effects are considered to be [undead](#undead).

### Wards ### {#wards}

All Ward spells are also [Chanting spells](#chanting).
Ward spells affect a specific type of creature listed in the name of the Ward.
When active, a Ward spell keeps the spellcaster from being attacked by creatures
affected by that spell. A Ward spell affects creatures in front of the
spellcaster, limited by a 180 degree hemisphere extending from shoulder to
shoulder and outwards from the chest. The Ward will not keep the targeted
creature(s) from walking around the spellcaster to attack others nearby. If the
Ward affects them, the targeted creature(s) must stay approximately five feet
away from the spellcaster, but need not retreat if the spellcaster advances upon
them. To cast a Ward, the spellcaster must hold their spell focus out toward the
targeted creatures while repeating the verbal. The verbal should make it clear
what creatures are affected by the Ward. For example, "Stay back undead. Stay
back undead. Stay back undead. Shoo," would be an appropriate verbal for the
[Ward: Undead](#ward-undead) spell. This spell will work for as long as the
spellcaster holds out the focus and keeps repeating the verbal. The people
playing the targeted creatures must be able to hear what the spellcaster is
saying, so it is up to the spellcaster to clearly and loudly chant their verbal.
While casting this spell, the spellcaster may not attack the targeted creatures.
A being that looks like it should be a targeted creature may not be (or may be
immune to the Ward). It is still the responsibility of the spellcaster to take
any and all weapon blows that hit them, even if the blows are from a creature
they believe should be affected by the Ward.

### Weapon Calls ### {#weapon-calls}

Each Weapon Call spell has a VC or AC which prepares the weapon with the special
combat call, using a casting of the spell. The casting is spent if the blow
lands an attack to a legal hit location. Otherwise, upon a parry or a miss, only
the preparation is lost and the use remains. If you are unsure that the blow
landed, you must assume that it did. The spellcaster cannot cast a different
Weapon Call spell on a currently enchanted weapon until the first spell has been
discharged. The spellcaster's weapon may not be used by anyone else and still
retain the enchanted status. If someone other than the spellcaster [attacks with]{.new} 
the prepared weapon, the preparation is lost, and must be reapplied.

[The spells that allow special combat calls are [Assassin's
Blade](#assassins-blade), [Armor-Piercing Weapon](#armor-piercing-weapon),
[Disease Weapon](#disease-weapon), [Enchant Weapon](#enchant-weapon), and
[Create Poison](#create-poison). These spells are mutually exclusive and cannot
be cast upon a weapon that already has a separate combat call. Weapon types,
such as axes or maces, are not covered by this caveat. None of these spells can
be cast upon a [Magic Missile](#magic-missile) or [Lightning
Bolt](#lightning-bolt-spell).]{.new}

## 6.6: Grandfathering ## {#grandfathering}

[When a player has been gone for a while, or when the rules have changed at the Event Holders' Council, a player might have a spellbook that is no longer legal by the rules of the Omnibus. When a player arrives at an event with an illegal spellbook, they must change their spellbook to match the current rules. This is referred to as grandfathering. Some years, the Event Holders' Council may decide to allow more permissive grandfathering, based on the changes that were made. If that happened this year, you will find details at the end of this section.]{.new}

[If a spell list has spells that no longer exist, or spells at circles that are no longer legal, remove those spells from the spell list. After this, the character may add spells from their Spell Mastery to replace removed spells. The spells added this way are subject to the usual rules about what spells can be learned at what circles. The number of spells they can add is dictated by how many they had before grandfathering; they may not end with more spells than they started with. This must result in a legal spellbook.]{.new}

[After making changes to their spellbook, a player grandfathering must check it in with the Event Holder or a designated Magic Marshal to get it signed. Though characters might learn or unlearn spells as part of grandfathering, it is a separate process intended for correcting illegal spellbooks. As such, grandfathering does not prevent a character from otherwise learning or unlearning spells at an event. If any spells were learned as part of grandfathering, the signature they receive for grandfathering counts as the signature(s) needed for those learned spells. Further, any spells learned may be added to the character's Spell Mastery section, if they have one.]{.new}

## 6.7: The Spells ## {#the-spells}

### 1st Circle Spells ### {#first-circle-spells}

::: {.columns-list}
* [Cure Disease](#cure-disease)
* [Detect Magic](#detect-magic)
* [Disrupt Light](#disrupt-light)
* [Fighter's Intuition](#fighters-intuition)
* [Ghost Blade](#ghost-blade)
* [Heartiness](#heartiness)
* [Identify](#identify)
* [Immunity to Poison](#immunity-to-poison)
* [Implement](#implement)
* [Light](#light)
* [Mentor](#mentor){.new}
* [Pas](#pas)
* [Protect Item](#protect-item)
* [Protection from Boulder](#protection-from-boulder)
* [Repair Armor](#repair-armor)
* [Speak](#speak)
* [Speak with Dead](#speak-with-dead)
* [Strange Brew](#strange-brew)
* [Zombie Walk](#zombie-walk)
:::

### 2nd Circle Spells ### {#second-circle-spells}

::: {.columns-list}
* [Aura of Protection](#aura-of-protection)
* [Death Watch](#death-watch)
* [Deep Pockets](#deep-pockets)
* [Disenchant](#disenchant)
* [Enchant Weapon](#enchant-weapon)
* [Group Healing](#group-healing)
* [Guidance](#guidance)
* [Heal Limb](#heal-limb)
* [Protect the Soul](#protect-the-soul)
* [Protection from Missile](#protection-from-missile)
* [Repair Item](#repair-item)
* [Ward: Undead](#ward-undead)
:::

### 3rd Circle Spells ### {#third-circle-spells}

::: {.columns-list}
* [Animate Lesser Undead](#animate-lesser-undead)
* [Beckon Corpse](#beckon-corpse)
* [Cantrip](#cantrip)
* [Commune with Spirit](#commune-with-spirit)
* [Disease Weapon](#disease-weapon)
* [Enfeeble Being](#enfeeble-being)
* [Feign Death](#feign-death)
* [Fortune Tell](#fortune-tell)
* [Precognition](#precognition)
* [Purity to Disease](#purity-to-disease)
* [Purity to Poison](#purity-to-poison)
* [Raise Dead](#raise-dead)
* [Skew Divination](#skew-divination)
* [Soul Bane](#soul-bane)
:::

### 4th Circle Spells ### {#fourth-circle-spells}

::: {.columns-list}
* [Animal Companion](#animal-companion)
* [Animate Undead](#animate-undead)
* [Armored Cloak](#armored-cloak)
* [Call the Soul](#call-the-soul)
* [Circle of Protection](#circle-of-protection)
* [Combat Raise Dead](#combat-raise-dead)
* [Create Poison](#create-poison)
* [Death Wish](#death-wish)
* [Disrupt](#disrupt)
* [Divine Aid](#divine-aid)
* [Enchant Armor](#enchant-armor)
* [Find the Path](#find-the-path)
* [Foretell](#foretell)
* [Magic Missile](#magic-missile)
* [Mystic Forge](#mystic-forge)
* [Séance](#seance)
* [Shapeshifting](#shapeshifting)
* [Transmute Self](#transmute-self)
:::

### 5th Circle Spells ### {#fifth-circle-spells}

::: {.columns-list}
* [Animate Undead General](#animate-undead-general)
* [Armor-Piercing Weapon](#armor-piercing-weapon)
* [Circle of Healing](#circle-of-healing)
* [Familiar](#familiar)
* [Reforge](#reforge)
* [Regenerate the Soul](#regenerate-the-soul)
* [Regeneration](#regeneration-spell)
* [Resist Magic](#resist-magic)
* [Vision](#vision)
* [Ward: Enchanted Beings](#ward-enchanted-beings)
:::

### 6th Circle Spells ### {#sixth-circle-spells}

::: {.columns-list}
* [Assassin's Blade](#assassins-blade)
* [Cry of Life](#cry-of-life)
* [Embrace Death](#embrace-death)
* [Intervention](#intervention)
* [Lightning Bolt](#lightning-bolt-spell)
* [Masterwork Hammer](#masterwork-hammer)
* [Prophecy](#prophecy)
* [Resist Death](#resist-death)
* [Ritual of Banishment](#ritual-of-banishment)
* [Second Chance](#second-chance)
* [Seed of Life](#seed-of-life)
* [Transformation](#transformation)
:::

## 6.8: Spell Descriptions ## {#spell-descriptions}

### Animal Companion (4th Circle) ### {#animal-companion}

__Uses:__ 1, and the spellcaster may only have one in-play - __Material:__ A
stuffed or toy animal that must be at least 4" tall - __Caveats:__
[Suspension](#suspension)

The spellcaster has an animal companion represented by a specific stuffed or toy
animal. The animal companion cannot be slain or disenchanted, but can be stolen.
The stuffed animal must be labeled with the spellcaster's name and the words
"[Event-Stealable](#event-stealable)." The animal companion grants two separate
abilities. The first ability is that the spellcaster may send their animal
companion to gather information once per learning of the spell. The information
gathered can only be relayed in a short sentence or concept, such as "the way
ahead is blocked," or "there are many foes," etc. This ability is represented by
the spellcaster giving their animal companion to the EH or MM and asking them a
question. If the spellcaster asks for information that their animal companion is
unable to obtain, they receive no answer.

The second ability that the animal companion grants is the ability to cast a
single spell up to third circle, except the spell [Implement](#implement), which
cannot be chosen. This spell will function as if it were learned normally, with
the same requirements, limitations, number of castings, and components. While
the animal companion is out gathering information the spells provided from the
secondary ability are suspended.

Any additional spells provided by the animal companion require the animal
companion to cast and maintain, as if the animal companion were a spell focus.
Each spell must meet the requirement for verbal, material and active components.
Spells with lasting effects (protections, immunities, etc.) can only be cast
upon the spellcaster. Any blow that strikes the animal companion must be taken
as if the animal companion is not there.

When the spellcaster first learns this spell, they choose their animal
companion's abilities. These abilities are not alterable from event to event.
The spellcaster must list in their spellbook every spell their animal companion
grants them as if they have learned the spell.

If they learn the spell additional times, the animal companion gets stronger.
They may alter the abilities of their animal companion upon completion of each
learning by giving it an additional spell and question. If the spellcaster
unlearns a use of the spell, the animal companion becomes weaker and must be
adjusted accordingly.

### Animate Lesser Undead (3rd Circle) ### {#animate-lesser-undead}

__Uses:__ 3 - __Verbal:__ 30 words, and an explanation - __Material:__ A tabard,
or sash which clearly states "Undead," "Skeleton," "Ghost" or the like, or an
appropriate mask - __Caveats:__ [Compulsion](#compulsion), [Undead](#undead)

This spell creates a generic undead creature that will follow commands given by
the spellcaster. It will only work on players; if cast on NPCs, they can refuse.
If an NPC refuses a casting of the spell, the casting is not spent. To cast it,
the spellcaster must get a corpse and recite the verbal while putting the sash
or tabard over the corpse's head, tabard-style, or while giving the corpse a
mask to wear. If there is already an appropriate MC on the person, you may
simply touch the recipient of the spell. The Lesser Undead is held at bay by
[Ward: Undead](#ward-undead) or [Ward: Enchanted
Beings](#ward-enchanted-beings), cannot cross a [Circle of
Protection](#circle-of-protection), and is killed in the normal fashion (no
special protections are gained). It takes all blows and is killed in the normal
fashion. It cannot use any armor or spells regardless of what the PC normally
has. It will obey simple commands exactly, but will rebel if given any commands
that violate the compulsion caveat. If left unattended, it will try to find the
spellcaster that created it. If the target is not raised by the end of the
event, they are considered dead and soulless. A character that is created as
undead can be slain and raised or simply raised while undead to return to life.

### Animate Undead (4th Circle) ### {#animate-undead}

__Uses:__ 2 - __Verbal:__ 30 words, and an explanation - __Material:__ A tabard,
or sash which clearly states "Undead," "Skeleton," "Ghost" or the like, or an
appropriate mask - __Caveats:__ [Compulsion](#compulsion), [Undead](#undead)

This spell, when cast upon a dead body, creates an undead creature that can use
any spells or weapons that the target could normally. The undead is obviously
not quite human, so it must either be made up to look a little odd, wear a mask,
or wear a sash that clearly states that the person is some kind of undead
creature. If this is already done you may simply touch the recipient of the
spell as you say the verbal. What kind of creature is left up to the
spellcaster, but it must definitely look slightly non-human. It does not allow a
spellcaster to break their weapon restrictions. The undead must obey most direct
commands given by the spellcaster, but is otherwise in full possession of their
faculties. The only commands that an undead creature can ignore are ones that
violate the compulsion caveat.

They are held at bay by [Ward: Undead](#ward-undead) or [Ward: Enchanted
Beings](#ward-enchanted-beings), cannot cross a [Circle of
Protection](#circle-of-protection), and are killed in the normal fashion (no
special protections are gained). The target is considered to be undead until
they are raised in a normal fashion. If [Animate Lesser
Undead](#animate-lesser-undead) is cast upon the target while the target is
still undead, it will raise the target as if another Animate Undead spell were
cast upon it, but the target's loyalties transfer to the person who cast
[Animate Lesser Undead](#animate-lesser-undead) (no additional MC is needed). If
a PC is still undead at the end of the event, that PC is considered dead and
soulless. This spell will not work on a body without its soul. A character that
is created as undead can be slain and raised or simply raised while undead to
return to life.

### Animate Undead General (5th Circle) ### {#animate-undead-general}

__Uses:__ 1 - __Verbal:__ 40 words, and an explanation - __Material:__ A tabard,
or sash which clearly states "Undead," "Skeleton," "Ghost" or the like, or an
appropriate mask, a written explanation of what this spell does, and you must
supply them with 3 MCs for the [Animate Lesser Undead](#animate-lesser-undead)
spells they will cast. - __Caveats:__ [Compulsion](#compulsion),
[Undead](#undead)

This spell, when cast upon a dead body, creates a greater undead creature that
can use any spells or weapons that the target could normally. The undead is
obviously not quite human, so it must either be made up to look a little odd,
wear a mask, or wear a sash that clearly states that the person is some kind of
undead creature. If this is already done you may simply touch the recipient of
the spell as you say the verbal. What kind of creature is left up to the
spellcaster, but it must definitely look slightly non-human. It does not allow a
spellcaster to break their weapon restrictions. The undead must obey most direct
commands given by the spellcaster, but is otherwise in full possession of their
faculties. The only commands that an undead creature can ignore are ones that
violate the compulsion caveat.

The Undead General is held at bay by [Ward: Undead](#ward-undead) or [Ward:
Enchanted Beings](#ward-enchanted-beings), cannot cross a [Circle of
Protection](#circle-of-protection), and is killed in the normal fashion (no
special protections are gained). The Undead General is considered to be undead
until they are raised in a normal fashion. If [Animate Lesser
Undead](#animate-lesser-undead) is cast upon the target while the target is
still undead, it will raise the target as if another [Animate Undead
General](#animate-undead-general) spell were cast upon it, but the target's
loyalties transfer to the person who cast [Animate Lesser
Undead](#animate-lesser-undead) (an MC is needed). The Undead General will die
to any magical attacks that damage it, regardless of location. If a PC is still
undead at the end of the event, that PC is considered dead and soulless. This
spell will not work on a body without its soul. A character that is created as
undead can be slain and raised or simply raised while undead to return to life.
If a character under the effects of [Embrace Death](#embrace-death) has this
cast on them, it acts as the spell [Animate Lesser
Undead](#animate-lesser-undead) instead. You may have at most one undead general
at a time.

In addition, the Undead General gains the ability to cast the spell [Animate
Lesser Undead](#animate-lesser-undead) with unlimited uses. An Undead General
can only have 3 Lesser Undead at a time and must retrieve their MC before
casting [Animate Lesser Undead](#animate-lesser-undead) again. If the Undead
General casts [Animate Lesser Undead](#animate-lesser-undead) on anyone who is
not under the effects of [Embrace Death](#embrace-death), they must use an MC.

### Armor-Piercing Weapon (5th Circle) ### {#armor-piercing-weapon}

__Uses:__ 4 - __Material:__ A cloth - __Active:__ Wipe the entire length of the
[weapon's striking surface]{.new} 5 times - __Caveats:__  [Enchanted Items](#enchanted-items), 
[[OOC Calls](#ooc-calls),]{.new} [Weapon Calls](#weapon-calls)

This spell gives the spellcaster the ability to enhance their weapon to pass
through and destroy armor. After preparing the spell the spellcaster must call
"[Armor-Piercing](#armor-piercing)" on the next [attack with that]{.new} weapon.

### Armored Cloak (4th Circle) ### {#armored-cloak}

__Uses:__ Unlimited, one at a time - __Verbal:__ 30 words - __Material:__ A
piece of garb with obvious runes or mystic symbols - __Active:__ Lie on back
while wearing the garment [- __Caveats:__ [OOC Calls](#ooc-calls)]{.new}

This spell enchants a single piece of garb to provide one call of armor against
an attack. It provides one point of armor against the next blow that lands upon
the garment or any location at least 75% covered by the garment.

This Armored Cloak cannot be worn in combination with any other form of armor,
ever. It can be worn while protected by a [Protection from
Missile](#protection-from-missile) or [Resist Magic](#resist-magic) spell, in
which case the wearer can choose to call either protection, saving the other for
later. It can only be worn by the spellcaster, and cannot be cast on the same
garment more than once. A specific piece of garb must be chosen for the MC at
the beginning of the event, and cannot be changed during the course of the event
without the EH's permission.

For each additional learning above the first, you may select another, separate
garment to enchant to share the single point. For instance, if you have taken
the spell twice, you may choose to enchant a cloak and a shirt, however it only
absorbs one blow on either hit location (not both). All garments must meet the
requirements of the spell, and are all charged through a single casting of the
spell.

### Assassin's Blade (6th Circle) ### {#assassins-blade}

__Uses:__ Unlimited, one at a time - __Material:__ A cloth, and either a single
weapon or arrow [within the spellcaster's weapon restriction]{.new}, labeled with "Assassin's Blade,"
"[Event-Stealable](#event-stealable)," and the spellcaster's name - __Active:__
Wipe the entire length of the blade 5 times - __Caveats:__ [Enchanted Items](#enchanted-items), 
[[OOC Calls](#ooc-calls),]{.new} [Weapon Calls](#weapon-calls)

This spell allows the spellcaster to prepare their MC with the spells
[Armor-Piercing Weapon](#armor-piercing-weapon), [Enchant
Weapon](#enchant-weapon), or [Create Poison](#create-poison), without expending
a casting, although a use of the spell must still remain in order to prepare it.
To prepare the weapon with one of the listed spells, they must wipe the blade of
the weapon with the cloth 5 times. Only one spell may be prepared at a time onto
the Assassin's Blade. Anyone may use the weapon, but only the spellcaster may
utilize the special call with it. The weapon is one-handed, but may not be used
in conjunction with another weapon or shield by the spellcaster. If the MC is IC
broken, it will retain its magical properties if repaired.

In addition, any body destroying blow dealt by an Assassin's Blade counts as two
blows dealt.

### Aura of Protection (2nd Circle) ### {#aura-of-protection}

__Uses:__ Unlimited, one at a time per learning - __Verbal:__ 20 words -
__Material:__ Spell Sash - __Active:__ Kneel or lie on back, no weapons in hand
- __Caveats:__ [Enchanted Items](#enchanted-items), [[OOC Calls](#ooc-calls),]{.new} 
[Spell Sash](#spell-sash)

The recipient of this spell lessens the effect of the next hit they take from a
call of "Magic," "Silver," "Disease," "Armor-Piercing," "Lightning Bolt," or
"Poison." Rather than taking the effect they were hit with, the recipient of the
spell takes the blow as a normal sword blow instead. It is necessary to call
"Protection" when the spell activates.

When the spell is cast, the recipient of the spell must be kneeling or be lying
on their back with no weapons in hand. This spell may be cast on a recipient
other than the spellcaster; to do so the spellcaster must have no weapons in
hand and touch the recipient while the spell is being cast. More than one
casting of this spell may be in effect on a single PC. If the spellcaster casts
this spell on another PC, they may not re-cast the spell until the sash is
returned to them.

### Beckon Corpse (3rd Circle) ### {#beckon-corpse}

__Uses:__ 5 - __Verbal:__ 20 words, repeated continuously, stating purpose of
spell - __Active:__ Spellcaster must be stationary until finished - __Caveats:__
[Chanting](#chanting), [Suspension](#suspension), [Undead](#undead), [Walking
Dead](#walking-dead)

This spell allows the spellcaster to summon a corpse to get up and move to them
as the [Walking Dead](#walking-dead). The spellcaster must first get the
attention of the player of said corpse and begin chanting the verbal. As long as
the chanting continues, the corpse will get up and walk in the most direct (but
OOC safe) path to the spellcaster as if under the effects of the spell [Zombie
Walk](#zombie-walk). If the corpse is interrupted, it will fall to the ground,
but the spellcaster may finish the current round of the verbal, regain the
corpse's attention, and resume chanting the verbal to renew the effect on the
corpse. The spell will end if the corpse reaches the spellcaster, the
spellcaster stops chanting to do something else, or they move from where they
are standing (although they may be moving their arms).

### Call the Soul (4th Circle) ### {#call-the-soul}

__Uses:__ 2 - __Verbal:__ 30 words - __Material:__ Five objects of equal size
and shape, four of one color or design and one of a second color or design, and
an opaque pouch or other way to keep them hidden - __Active:__ A quest may be
required

Allows the spellcaster to possibly find and reattach the soul of a soulless
character. When cast, the spellcaster presents the pouch of objects to the
soulless character, who must then reach in and take one without looking. If the
object is one of the four, the soul is reattached. If the object is the second
color or design, nothing happens. This spell must be cast in the presence of the
MM, who may require an additional quest be completed.

### Cantrip (3rd Circle) ### {#cantrip}

__Uses:__ 3

Allows the spellcaster to gain one casting of any First Circle spell, chosen at
the time of casting. The spell gained must be cast following the rules for that
spell, including VC, MC, and ACs. This spell cannot be used to cast
[Implement](#implement) or [Strange Brew](#strange-brew).

### Circle of Healing (5th Circle) ### {#circle-of-healing}

__Uses:__ 1 - __Verbal:__ 25 words - __Material:__ 10-foot rope - __Active:__
Touch the rope - __Caveats:__ [Circles](#circles), [Suspension](#suspension)

This spell allows the spellcaster to create a Circle of Healing. The spellcaster
may name the circle with [Cure Disease](#cure-disease), [Heal Limb](#heal-limb),
or [Raise Dead](#raise-dead), chosen at the time of casting. While the
spellcaster is standing in the Circle of Healing, they may cast the named spell
as many times as desired without consuming a use of that spell. Except for the
MC of Cure Disease, all of the requirements of the named spell must be met for
each casting, including the AC and VC. No one but the spellcaster may use the
Circle of Healing in this manner. This spell is broken if a weapon crosses the
plane of the circle. For this purpose, a weapon is considered to be anything
with a legal striking surface - therefore, swords and arrows are weapons,
although bows and shields are not.

### Circle of Protection (4th Circle) ### {#circle-of-protection}

__Uses:__ Unlimited, one at a time - __Verbal:__ 10 words - __Material:__
15-foot white rope, or less - __Caveats:__ [Circles](#circles),
[Suspension](#suspension), [Enchanted Beings](#enchanted-beings)

This spell creates a barrier that no enchanted being can physically pass,
affect, or attack, with the exception of the spellcaster that cast it. In
addition, no magic of any kind can pass through the barrier in either direction,
again with the exception of the spellcaster that cast the spell. The spellcaster
that cast the circle may pass over the barrier freely and may cast spells
through the barrier at will.

A single spellcaster with multiple castings, or several different spellcasters,
can combine Circle of Protection spells to make a larger one. If multiple
spellcasters join their circles together, the result is a larger circle, but no
enchanted being (even the spellcasters that created it) may pass in or out of
the circle, and no magic of any kind (even from the spellcasters that created
it) can pass through the barrier in either direction. Magic can be cast inside
the circle as normal, but all the effects remain within the circle.

The spellcaster may decide to break their own circle whenever they choose by
uncrossing the rope. The spellcaster can do this even if the circle was cast
with other spellcasters.

### Combat Raise Dead (4th Circle) ### {#combat-raise-dead}

__Uses:__ 3 - __Verbal:__ 3 words - __Active:__ Must touch recipient of spell

This spell will raise a dead character, healing all of their injured limbs. The
VC must clearly state the effects of the spell. For example, "Rise and fight" is
a VC that would make it clear that the individual is being raised.

### Commune with Spirit (3rd Circle) ### {#commune-with-spirit}

__Uses:__ 1 - __Active:__ Ritual - it is required that the spellcaster not
actively seek out the MM or EH. The ritual must, in effect, be spectacular
enough that the EH or MM comes of their own volition.

Allows the spellcaster to gain a boon of insight/wisdom from the EH. This spell
allows the spellcaster to ask a spirit something relating to the plot of the
event. How detailed the response or how lengthy the conversation is with the
spirit, is determined completely by the spirit. It should be noted that the
spirit does not have to answer the call of the spellcaster, and that sometimes
instead of helping solve a problem, may give the spellcaster an awareness of
more problems that need solving.

### Create Poison (4th Circle) ### {#create-poison}

__Uses:__ 6 - __Material:__ Disposable edible or drinkable component -
__Caveats:__ [Enchanted Items](#enchanted-items),
[Compulsion](#compulsion), [[OOC Calls](#ooc-calls), [Weapon Calls](#weapon-calls)]{.new}

This spell creates one dose of a specific kind of poison per use. There are only
four types of poison created by this spell: [Charm]{.new}, Death, Sleep, and Truth.

* [Charm makes the victim believe the first person they see is their best friend]{.new} 
  for the next 10 minutes.
* Death kills instantly.
* Sleep causes the victim to fall into a deep sleep for 10 minutes. They cannot
  be woken before this time passes.
* Truth causes the victim to be unable to speak anything except the truth for 10
  minutes, but does not force them to talk.

The type of poison must be chosen when it is cast, and each individual effect
listed in the spellcaster's spellbook. The poison must be ingested and cannot
affect those who don't eat or drink it. This effect must be written legibly on a
scroll. This scroll is to be given to the victim by the spellcaster or MM
immediately after the MC has been consumed. Only one dose may be made per use,
and only the first person to ingest part of the MC is affected by a spell. The
spell is always rendered inert by [Immunity to Poison](#immunity-to-poison).
While the poison may have the intended effect to alter how someone may act, such
as a [charm poison]{.new}, it may in no way be used to compel any player to act in what
they consider an immoral or unethical fashion. A person who ingests a [charm poison]{.new} 
always has the option of allowing their PC to die if they are OOC
uncomfortable with the situation. In any case, you should talk with the EH or MM
if the use of any poison bothers you.

Alternatively the [spell]{.new}caster may expend a use of the spell to coat a weapon with
deadly poison. The next time the [spell]{.new}caster [attacks with that]{.new} weapon they must call
"[Poison](#poison)." To use this spell this way, the MC becomes a cloth, and the
AC becomes wipe the length of the [weapon's striking surface]{.new} 5 times with the MC.

### Cry of Life (6th Circle) ### {#cry-of-life}

__Uses:__ 1 - __Verbal:__ "All in the sound of my voice, rise and fight."

This spell instantly raises all dead characters whose players hear the verbal.
The spell affects all who hear it, including NPCs and characters fighting
against the spellcaster.

### Cure Disease (1st Circle) ### {#cure-disease}

__Uses:__ 5 - __Verbal:__ 20 words - __Material:__ Disposable

This spell will cure the recipient of all diseases that are currently affecting
them. It will not provide protection from catching a disease after the spell is
cast.

### Death Watch (2nd Circle) ### {#death-watch}

__Uses:__ Unlimited, one at a time - __Active:__ Spellcaster must sit without
weapons in-hand for 60 seconds before they are killed.

Enables the spellcaster to see and hear while they are dead. You may not speak
or move while dead, except for addressing marshaling calls or OOC
unsafe/uncomfortable situations. If the spellcaster is rendered soulless. all
memories acquired through the current casting of Death Watch are erased (i.e.,
all memories acquired from the time of your PC's last death). The spell ends
when the spellcaster is raised (either as alive or as undead).

### Death Wish (4th Circle) ### {#death-wish}

__Uses:__ 2 - __Verbal:__ 30 words, and an explanation - __Material:__ A scroll
containing the trigger phrase and command __Caveats:__ [Compulsion](#compulsion)

This spell, when cast upon a dead body, implants a simple command into their
mind. The spellcaster must give the target player the scroll after completing
the verbal. The target should read the scroll and may refer to it at any time.
However, the scroll and the information it contains are OOC, and are not known
to the PC in any way. When the target character is alive, and hears the trigger
phrase, they must perform the command to the best of their ability. The spell
ends when the target successfully completes the command, or is slain trying. The
target may ignore any commands that violate the compulsions caveat.

This spell will not work on a body without its soul. The spell [Protect the
Soul](#protect-the-soul) negates all effects of this spell.

### Deep Pockets (2nd Circle) ### {#deep-pockets}

__Uses:__ 3 - __Material:__ A bag no larger than 6" by 12" by 3"

Each casting allows spellcaster to deny any stealable items that are completely
within the MC to the next three characters that search the spellcaster, while
that bag is on the spellcaster's body. If the spellcaster is not carrying any
stealable items outside of the bag, they may answer, "Nothing." All other
stealable items must be yielded to a search. Each additional learning of this
spell allows the spellcaster an additional 6" by 12" by 3" volume of bag, either
as a separate bag, or a larger bag. However, the spellcaster can only have one
usage cast at a time. Each search denial is used up on all of the volumes
simultaneously. One Deep Pockets bag may never contain another. No matter how
many Deep Pockets castings are combined it does not combine the amount of people
that need to search it; it only increases the size of the bag.

### Detect Magic (1st Circle) ### {#detect-magic}

__Uses:__ 5 - __Verbal:__ 20 words

This spell allows the spellcaster to take any one item (not a living or dead
creature) to the EH or MM to ask whether casting [Identify](#identify) upon the
object will yield any information the spellcaster cannot determine by looking at
it, such as "It's a stick," or "It's a sword." It may be cast on a living or
dead being to detect a magical item it carries, such as a spell focus or magic
weapon. In this case, it will not tell what the item does, only that it is there
and which item it is. If cast in this manner multiple times and there are
multiple magical items, it will not repeat magic items until there are no new
items to reveal.

### Disease Weapon (3rd Circle) ### {#disease-weapon}

__Uses:__ 3 - __Verbal:__ 10 words - __Material:__ Spellcaster's weapon -
__Caveats:__ [Enchanted Items](#enchanted-items), [[OOC Calls](#ooc-calls),]{.new}
[Weapon Calls](#weapon-calls)

This spell allows the spellcaster to temporarily enchant their weapon. After
preparing it with the spell, it is considered a diseased weapon and the
spellcaster must call "[Disease](#disease)" on the next [attack with that]{.new} weapon.

### Disenchant (2nd Circle) ### {#disenchant}

__Uses:__ 2 - __Verbal:__ 30 words - __Active:__ Touch the target item

This spell will remove enchantments from the target item. If the target item is
a potion, panacea, or scroll, it will be rendered inert. If the target is a
magic weapon it will no longer function as such until repaired by a
[Reforge](#reforge) spell. If the target is a [Spell Sash](#spell-sash), then
the spell represented by the sash is ended. [Circle of
Protection](#circle-of-protection), [Light](#light), and other pure spells are
not affected unless otherwise stated in their description. Only items specified
by the EH are immune to this spell.

### Disrupt (4th Circle) ### {#disrupt}

__Uses:__ 5 - __Verbal:__ 30 words, starting with "I disrupt this (spell name)
..." - __Active:__ Clearly point at the target - __Caveats:__
[Suspension](#suspension)

This spell will suspend any [circle](#circles) or [chanting](#chanting) spell.
It may only be cast upon a spell that is
currently in use. Once the spellcaster completes the disruption, the target
spell is suspended for five minutes and the spellcaster of the target spell
loses the ability to cast the target spell for five minutes. If the target spell
ends before the disruption is completed (the spellcaster stops chanting, the
circle is broken, etc.), the spellcaster of that spell still loses the ability
to cast that spell for five minutes. This spell only stops the current learning
of the target spell. Therefore, if the spellcaster has taken [Ward: Enchanted
Beings](#ward-enchanted-beings) twice, they temporarily lose the ability to cast
one, but retain the ability to cast the other.

### Disrupt Light (1st Circle) ### {#disrupt-light}

__Uses:__ 5 - __Verbal:__ 20 words, which must clearly state the effect of the
spell

This spell cancels [Light](#light) spells cast by other spellcasters. Once the
Disrupt Light spellcaster is within sight and hearing of a [Light](#light)
spellcaster, they may loudly call out their verbal. Upon completion of the
verbal, all other spellcasters within hearing range must put away their active
[Light](#light) spells. This action is OOC, and those affected must do so even
if they hear the spell while dead. Spellcasters so affected cannot recast the
[Light](#light) spell for five minutes, after which time they may reuse the same
chemical light sticks.

### Divine Aid (4th Circle) ### {#divine-aid}

__Uses:__ 1 - __Verbal:__ Speak to EH - __Material:__ A sacrifice may be
required - __Active:__ A quest may be required

This spell allows the spellcaster to send a request for aid to a higher power.
The request cannot be specific and the higher power may send whatever aid they
see fit. This spell comes with no guarantee that the EH won't simply listen to
the request and say "No." This spell cannot create an effect that will last
beyond the end of the event, other than for healing purposes. A spellcaster who
uses drama and theatrics has a better chance of success.

### Embrace Death (6th Circle) ### {#embrace-death}

__Uses:__ 1 - __Verbal:__ 40 words - __Material:__ A container at least 4 inches
in diameter able to hold the spellcaster's soul token, labeled with
"Event-Stealable". The container cannot be placed into [Deep
Pockets](#deep-pockets). - __Caveats:__ [Enchanted Items](#enchanted-items),
[Regeneration](#regeneration-caveat), [Basic Regeneration](#basic-regeneration),
[Undead](#undead)

This spell allows the spellcaster to remove their soul from their body, giving
them the ability to defy death. The spellcaster's soul token is the only thing
which may be stored in the MC (hereafter referred to as the phylactery). When
the spell is learned, the spellcaster must scribe in their spellbook a
description of exactly what the phylactery looks like. Upon casting this spell,
the spellcaster places their soul token within the phylactery, which becomes an
[event-stealable](#event-stealable) item for the duration of the spell. The
spellcaster may do whatever they want with the phylactery: place it in a mundane
pocket, hide it, give it to someone, etc. However, the phylactery may not be put
into [Deep Pockets](#deep-pockets).

While under the effects of this spell, the spellcaster is [undead](#undead)
and is not affected by poisons, diseases, or the
spells [Call the Soul](#call-the-soul), [Cry of Life](#cry-of-life), [Heal
Limb](#heal-limb), [Potion of Combat Raise Dead](#potion-of-combat-raise-dead),
[Potion of Heal Limb](#potion-of-heal-limb), [Regenerate the
Soul](#regenerate-the-soul), [Regeneration](#regeneration-spell), and [Seed of
Life](#seed-of-life). Additionally, the spells [Raise Dead](#raise-dead) and
[Combat Raise Dead](#combat-raise-dead) will only affect the spellcaster if the
character casting these spells is touching the phylactery. Being raised in this
manner returns the spellcaster to undeath and repairs all of their damaged
limbs.

While this spell is active, the spellcaster's limbs may be regenerated by
remaining stationary for 30 seconds. The spells [Animate
Undead](#animate-undead), [Animate Undead General](#animate-undead-general), and
[Animate Lesser Undead](#animate-lesser-undead), as well as [Potion of Animate
Lesser Undead](#potion-of-animate-lesser-undead) and [Potion of Animate
Undead](#potion-of-animate-undead) from the spell [Strange Brew](#strange-brew),
will return the spellcaster to undeath (repairing any damaged limbs), without
placing the spellcaster into someone else's control. While in possession of the
phylactery, the spellcaster may enchant themselves to regenerate (as described
below) by sitting without weapons in hand and incanting the VC. [They may not cast
any spells with Verbal Components while doing this.]{.new} This grants a
[basic regeneration](#basic-regeneration) that will return the spellcaster to
undeath the next time they die. If the spellcaster is killed with magic, the
regeneration time will double to 240 seconds. Once returned to undeath the
spellcaster must re-enchant themselves as described above, before they will
regenerate again in this manner.

Destruction of their body will prevent the spellcaster from regenerating, but
they may still be returned to undeath by any other means described previously.
The spellcaster does not hand over their soul token when their body is destroyed
while under the effects of this spell.

The spellcaster may end this spell at any time by opening the phylactery,
returning the soul to their body. If the phylactery is disenchanted (by use of
the [Disenchant](#disenchant) spell or a [Potion of
Disenchant](#potion-of-disenchant)), the spell is forcibly ended, and the
spellcaster is slain and considered soulless. If the spell is not ended before
the end of the event, the spellcaster is considered dead and soulless.

### Enchant Armor (4th Circle) ### {#enchant-armor}

__Uses:__ 1 - __Verbal:__ 30 words and an explanation - __Material:__ A
non-stealable token with the spellcaster's name and the words "Enchant Armor" on
it - __Caveats:__ [Enchanted Items](#enchanted-items)

[This spell creates a magical token that allows the spellcaster to continually repair 
armor worn by a specified character without expending other spells. When the spellcaster 
casts this spell, they give the token to the specified character, who must keep the 
token with them for the spell to remain in effect. Once this spell is cast, it may be 
forcefully ended by disenchanting the token. As long as the specified character has 
the token, and the spell has not ended, the spellcaster may cast 
[Repair Armor](#repair-armor) as an unlimited effect on any armor worn by the 
specified character by performing that spell's AC on the targeted hit location. 
This spell does not require the [spell]{.new}caster to currently have the spell 
[Repair Armor](#repair-armor) or have any remaining castings of 
[Repair Armor](#repair-armor) in order to use this effect.]{.new}

### Enchant Weapon (2nd Circle) ### {#enchant-weapon}

__Uses:__ 5 - __Verbal:__ 10 words - __Active:__ Hold the target weapon with
both hands - __Caveats:__ [Enchanted Items](#enchanted-items), 
[[OOC Calls](#ooc-calls), [Weapon Calls](#weapon-calls)]{.new}

This spell gives the spellcaster the ability to temporarily enchant a weapon,
arrow, or bow. After preparing it with the spell, the weapon, bow, or arrow user
must call "[Magic](#magic-call)" or "[Silver](#silver)" the next 3 times they
swing that weapon, the next 3 times they fire that bow, or the next time they
fire that arrow. The [spell]{.new}caster chooses which option, Magic or Silver, upon casting
the spell, and must inform the recipient which option they are imbuing it with.
These calls are expended whether the user scores a successful hit or not. If an
enchanted arrow is fired by an enchanted bow, a call is expended from both the
bow and arrow. If an arrow is enchanted with a particular call and fired from a
bow with a different call, the wielder may choose which call to use; regardless
of the call chosen, a call is expended from both the bow and the arrow.

### Enfeeble Being (3rd Circle) ### {#enfeeble-being}

__Uses:__ 2 - __Verbal:__ 30 words, starting with "I declare you mundane ..."

This spell allows the spellcaster to remove the special powers and abilities
from a single NPC creature. To cast the spell, the spellcaster must get the
creature's attention and begin the verbal. Once the spell is completed, the
target [may]{.new} lose [access to some or all of its]{.new} abilities. This 
includes natural armor, spells, regeneration, etc. Because this is a relatively 
low-circle spell, it will probably [have little or no effect on]{.new} more 
powerful creatures, such as unique enemies or the proverbial "Big Bad Guy," but 
it might work on things like a troll, a lesser demon, or a goblin shaman. This 
spell will never work on PCs. A spellcaster should choose their targets wisely.

### Familiar (5th Circle) ### {#familiar}

__Uses:__ 1. The spellcaster may only have one in-play - __Material:__ A stuffed
or toy animal that must be at least 4" tall, labeled with the spellcaster's
name and the words "Event-Stealable" - __Caveats:__ [Suspension](#suspension)

The spellcaster has a familiar that grants them more spell potential,
represented by a specific stuffed or toy animal. The familiar cannot be slain or
disenchanted, but can be stolen.

Any additional spells provided by the familiar require the presence of the
familiar to cast it and maintain it, as if the familiar were a spell focus or
additional MC. If the familiar is stolen or dropped, the spellcaster may not
cast or maintain any of the extra spells. Each spell must meet the requirement
for VC, MC, and ACs. Spells with lasting effects (protections, immunities, etc.)
can only be cast upon the spellcaster. Spells with lasting effects are suspended
while the familiar is not in the spellcaster's possession. Any blow that strikes
the familiar must be taken as if the familiar is not there.

The spellcaster has 5 points to spend on extra spells for every casting of this
spell, up to 20 points total.

+---------------------------------+-------------------+
| Benefit                         | Cost              |
+=================================+===================+
| [Raise Dead](#raise-dead)       | (1 pt.) - 1 use   |
+---------------------------------+-------------------+
| [Disrupt](#disrupt)             | (1 pt.) - 1 use   |
+---------------------------------+-------------------+
| [Call The Soul](#call-the-soul) | (2 pts.) - 1 use  |
+---------------------------------+-------------------+
| [Enfeeble                       | (2 pts.) - 1 use  |
| Being](#enfeeble-being)         |                   |
+---------------------------------+-------------------+
| [Find The Path](#find-the-path) | (2 pts.) - 1 use  |
+---------------------------------+-------------------+
| [Shapeshifting](#shapeshifting) | (2 pts.) - 1 use  |
+---------------------------------+-------------------+
| [Combat Raise                   | (3 pts.) - 2 uses |
| Dead](#combat-raise-dead)       |                   |
+---------------------------------+-------------------+
| [Reforge: Restore               | (4 pts.) - 1 use  |
| Enchantment](#reforge)          |                   |
+---------------------------------+-------------------+

When the spellcaster first casts this spell, they choose their familiar's
abilities. These abilities are not alterable from event to event. The
spellcaster must list every spell their familiar grants them in their spellbook
as if they have learned the spell.

If they learn the spell Familiar additional times, they may alter the abilities
of their familiar upon completion of each learning. If the spellcaster unlearns
a use of the spell Familiar, the familiar they have then becomes weaker and must
be adjusted accordingly.

### Feign Death (3rd Circle) ### {#feign-death}

__Uses:__ Unlimited - __Material:__ A cloth - __Active:__ Wipe cloth over face 5
times

This spell allows the spellcaster to disguise themselves so as to appear dead.
If someone asks them if they are dead they can legally answer "Yes," and may lie
down or sit with their sword or arm above their head as to appear dead (see
[rules on]{.new} [character death](#character-death) and [soul
loss](#soul-loss)). Feign Death ends once the spellcaster moves or speaks
(except for addressing marshaling calls or OOC uncomfortable/unsafe situations).
If a person moves them, thinking they are dead, the Feign Death does not end;
only when they move themselves. If struck while using Feign Death, the
spellcaster is still affected by the blow as normal.

### Fighter's Intuition (1st Circle) ### {#fighters-intuition}

__Uses:__ 3 - __Material:__ Spell Sash - __Active:__ place sash on fighter -
__Caveats:__ [[Enchanted Items](#enchanted-items),]{.new} [Spell Sash](#spell-sash)

This spell must be cast on a non-spellcaster by placing the sash on the fighter
and giving an explanation of how it works. This does not make the fighter an
enchanted being.

This fighter may now call out "Fighter's Intuition" once. When the fighter does
this, they may or may not learn information about a monster they can see. It is
up to the event staff to decide to provide this information or not. The
information can be anything: weakness, methods of defeating, or even what the
NPC likes to eat.

### Find the Path (4th Circle) ### {#find-the-path}

__Uses:__ 1 - __Verbal:__ 30 words - __Caveats:__ [Spell
Failure](#spell-failure)

This spell provides the spellcaster a route to find, locate, or travel to a
person, place or thing that they know by name. For instance, you can get a
response from "Where is the body of King Joe?" but not from "Take me to the
person who stole my sword." The results of this spell can come as a guide, a
map, a set of directions, a divining rod, or any other mechanic that the EH or
MM deems appropriate. Be aware that the answer may not always be the safest or
shortest path. This spell will fail if an answer cannot be determined because of
PC action.

### Foretell (4th Circle) ### {#foretell}

__Uses:__ 1 - __Verbal:__ A prediction that the EH/MM must be present for

Allows the spellcaster to make a prediction of an event to come, ie. "Sir Thomas
will slay a dragon with a silver sword." If the event foretold comes to pass the
EH/MM may grant a boon to the spellcaster or anyone involved in the prediction.
The nature and power of this boon is up to the EH/MM. The greater the foretold
event or more specific the prediction, the more powerful the resulting boon
should be.

### Fortune Tell (3rd Circle) ### {#fortune-tell}

__Uses:__ 2 - __Material:__ Fortune-telling paraphernalia, such as runes or a
tarot deck [- __Caveats:__ [Spell Failure](#spell-failure)]{.new}

This spell allows the spellcaster to ask a question of the EH or MM, which will
be answered in a symbolic manner. How much information (if any) and the form in
which it is given is at the discretion of the EH or MM. As this is a relatively
low-circle spell, no proper names may be used in either the question or the
answer for this spell. For example, while a spellcaster cannot ask "Who killed
Sir Schlep?" they can ask, "Who killed this knight?" and the answer can be
"Tarot Card: Jack of Wands" but not "Bad Bart." This spell can only be used to
determine information that is plot-related. If the EH or MM does not know the
answer because the question asked relates to PC actions, an answer will not be
given but the spell is still used. If the spell is cast and an answer cannot be
given because of any of the above limitations, the casting is still used up.

### Ghost Blade (1st Circle) ### {#ghost-blade}

__Uses:__ [2]{.new} - __Verbal:__ 20 words - __Material:__ A white ribbon with the words
"Ghost Blade" on it - __Caveats:__ [Enchanted Items](#enchanted-items)

This spell enchants a single weapon to no longer affect the casting of the
spells Raise Dead or Regeneration or the breaking of Circle of Healing. Upon
casting this spell, the spellcaster must tie the MC onto the enchanted weapon.

### Group Healing (2nd Circle) ### {#group-healing}

__Uses:__ 2 - __Verbal:__ 10 words - __Material:__ 30' rope - __Active:__ Touch
the rope - __Caveats:__ [Circles](#circles)

This spells allows the spellcaster to cast an enchanted circle. This circle
allows certain spells cast into it to affect all the people within the circle.
The Group Healing circle may be used to enhance the power of the following
spells: [Combat Raise Dead](#combat-raise-dead), [Cure Disease](#cure-disease),
[Immunity to Poison](#immunity-to-poison), [Heal Limb](#heal-limb), and [Raise
Dead](#raise-dead). Multiple castings of Group Healing from the same or
different spellcasters may be used at the same time, creating a bigger circle.

To enchant the Group Healing circle, lay the rope(s) in a circle on the ground
with the ends touching. Then all the characters to be cast upon should be
gathered into the circle. The spellcaster(s) must then recite the VC, which
empowers the circle.

The next spell from the accepted list cast into this circle by any spellcaster
affects all within as if it had been cast on each individually. If the spell has
any MC, only one is used.

### Guidance (2nd Circle) ### {#guidance}

__Uses:__ 2 - __Material:__ Divining paraphernalia to indicate a yes/no answer
[- __Caveats:__ [Spell Failure](#spell-failure)]{.new}

This spell allows the spellcaster to ask the EH or MM a yes/no question. If the
EH or MM does not know the answer because the question asked relates to PC
actions, an answer may not be given but the spell is still used. An answer will
be given in the form of "Yes" or "No" by the EH or MM. If the spell is cast and
an answer cannot be given because of any of the above limitations, the casting
is still used up.

### Heal Limb (2nd Circle) ### {#heal-limb}

__Uses:__ Unlimited - __Verbal:__ 20 words - __Active:__ Spellcaster must be
stationary, must touch the target limb

This spell allows the spellcaster to heal one damaged limb at a time. The
spellcaster must recite the VC while touching the recipient's injured limb. The
spellcaster cannot move their feet while casting this spell, although they may
be moving their arms (e.g., parrying, so long as they don't step backwards).

### Heartiness (1st Circle) ### {#heartiness}

__Uses:__ Unlimited, one at a time

Having this spell makes it harder to destroy the spellcaster's body. The next
time the spellcaster's body is destroyed it will take 200 extra blows to
successfully destroy their body. If struck for only 200 blows, instead of the
full 400 blows, the spellcaster must inform the individual(s) destroying their
body that "The job is not yet done." A spellcaster can only be under the effect
of one Heartiness spell at a time. A use is considered to be over whenever the
spellcaster receives at least 200 body destroying blows, but is in effect until
either their body is destroyed or they are raised.

### Identify (1st Circle) ### {#identify}

__Uses:__ 3 - __Verbal:__ 30 words

This spell allows the spellcaster to take any one item (not a living/dead
creature) to the EH or MM to ask what it is and expect an answer. This spell can
also determine what race an unknown creature is. If the spellcaster can
successfully reach visual inspection range, recite the verbal, and the creature
is not hostile, it must state what race it is. The response is not IC speech by
the creature, and it can answer while dead.

### Immunity to Poison (1st Circle) ### {#immunity-to-poison}

__Uses:__ 3 - __Verbal:__ 10 words - __Material:__ Disposable
[- __Caveats:__ [OOC Calls](#ooc-calls)]{.new}

This spell makes the recipient immune to the next dose of poison that would have
otherwise affected their PC during the event. When damaged by the next poison
attack, whether ingested or delivered by a poisoned weapon, call "Immunity to
Poison!" Only one Immunity to Poison is used at a time. The recipient must take
any mundane damage from a poisoned weapon regardless of whether they are
protected from the actual poison. The recipient should be given the MC when the
spell is cast, and they should dispose of it when the immunity has been used.
More than one Immunity to Poison can be cast upon a recipient; the effect is
stackable. The MC of the spell is not stealable or transferable after it is
cast. This spell can also be cast as an antidote for any one poison that the
recipient has been subjected to, but in this case it will not provide any
further protection.

### Implement (1st Circle) ### {#implement}

__Uses:__ Special - __Material:__ Safe, non-weapon Staff (between 4' and 6'
long, inclusive), Wand (between 12" and 18" long, inclusive), Orb (at least 4"
in diameter), or Book (bound, minimum 1/2" x 4" x 7", cannot be the
spellcaster's spell-book)

The spellcaster is able to create a staff, wand, orb, or book (hereafter called
"implement") that enhances their own spells. Each time the spellcaster learns
this spell, they gain 1 point into a pool from which they may purchase special
abilities from the following choices below. A spellcaster may only have 5 points
worth of implements per event. At the magic check-in of an event, the
spellcaster may choose how the points in their pool are spent.

Unless otherwise stated, abilities gained that augment or alter spells require
that the spellcaster already knows that spell, otherwise there is no effect. In
order to use the gained ability, the spellcaster must be holding the implement
in one hand.

The implement is a magical manifestation and cannot be broken. Any blow that
strikes the implement must be taken as if the implement is not there. You can
not actively parry with an implement.

An implement may be disenchanted causing any effects or castings to be lost
until the implement is restored. If disenchanted it takes [120]{.new} seconds of holding
the implement with both hands and nothing else to restore.

Gain one additional casting of one of the following spells for 1 point each:
[Find the Path](#find-the-path), [Fortune Tell](#fortune-tell),
[Guidance](#guidance), [Precognition](#precognition), [Skew
Divination](#skew-divination), [Raise Dead](#raise-dead), [Deep
Pockets](#deep-pockets), [Enfeeble Being](#enfeeble-being), [Beckon
Corpse](#beckon-corpse), [Disenchant](#disenchant), or [Disrupt](#disrupt).

Gain the following abilities for 1 point each:

* The AC for [Armored Cloak](#armored-cloak) is changed to "Kneel on one knee
  while holding their implement with both hands."
* When using a Circle spell you may double the length of the rope. This may only
  be done once.
* The AC for [Death Watch](#death-watch) is changed to "Spellcaster must kneel
  on one knee holding their Implement with both hands for 60 seconds before
  being killed." Additionally, the spell no longer ends when the spellcaster is
  raised.
* The spell [Death Watch](#death-watch) allows the [spell]{.new}caster to move their head
  while dead. They still may not speak, try to communicate, or move in any other
  way while dead.

Gain one additional casting of one of the following spells for 2 points each:
[Call the Soul](#call-the-soul), [Group Healing](#group-healing), [Resist
Magic](#resist-magic), [Animate Undead](#animate-undead), or [Soul
Bane](#soul-bane).

Gain the following abilities for 2 points each:

* The uses for [Speak](#speak) become unlimited.
* Gain 1 point to spend on your [familiar](#familiar). This ability may not be
  taken more than once. When this augmentation is first used through Implement,
  you shall list an alternate build for the [familiar](#familiar). This
  alternate build can only be changed by learning or unlearning a use of
  [Familiar](#familiar). From now on, when you use your [Familiar](#familiar)
  and it is augmented by Implement, it uses that new build. If you are using the
  augmented build, and you do not have your implement on you, you may not use
  your [Familiar](#familiar) abilities.

Gain one additional casting of one of the following spells for 3 points each:
[Vision](#vision) or [Séance](#seance).

Gain the following abilities for 3 points each:

* Gain one use of [Regeneration](#regeneration-spell). You are not required to
  know the spell to use this ability.
* Gain one use of [Regeneration](#regeneration-spell). Upon completion, the
  spellcaster will be raised as a free-willed undead. You are not required to
  know the spell to use this ability.
* Gain one additional [Magic Missile](#magic-missile) prop.

### Intervention (6th Circle) ### {#intervention}

__Uses:__ 1 - __Verbal:__ Speak to EH - __Material:__ A sacrifice may be
required - __Active:__ A quest may be required
[- __Caveats:__ [Spell Failure](#spell-failure)]{.new}

This spell allows the spellcaster to go to the EH and ask a boon from whatever
powers their magic. It should be cast in the presence of the EH or MM. It is to
be used to request favors such as: "Oh, please, great majestic god/Fire
Spirit/Navel Lint, grant me a quest to search for the lost soul of my overlord,
Sir Biff of Bonehead Ridge." This spell comes with no guarantee that the EH
won't simply listen to the request and say "No." This spell cannot create an
effect that will last beyond the end of the event, other than for healing
purposes. A spellcaster who uses drama and theatrics has a better chance of
success, and simple, small requests are also more likely to be granted. Any
requests that will unbalance the game will likely be either denied straight out,
or assigned an unsolvable quest.

### Light (1st Circle) ### {#light}

__Uses:__ Unlimited - __Verbal:__ 3 syllables - __Material:__ Chemical light
stick and dark bag [or EH/MM-approved electronic light with an on/off 
mechanism]{.new} - __Active:__ Snap and shake the [chemical light]{.new} stick 
[or turn on the electronic light]{.new}

This spell creates light. The spellcaster may use as many [Light props]{.new} 
as desired. [Electronic light sources must be checked in before use and may be 
pulled if they may be too bright or unsafe for the event. If using chemical light 
sticks, the spellcaster]{.new} must also carry a bag large enough to hold all of 
the glow sticks they will use and thick enough to prevent any light from escaping. 
The bag is to be used if they are affected by a [Disrupt Light](#disrupt-light) 
spell. The spellcaster may not give a [Light prop]{.new} to anyone who is going to 
travel beyond easy speaking distance. It is possible for this spell to be disrupted. 
It is the spellcaster's responsibility to know what the Disrupt Light spell is, how 
to recognize it, and how to respond to it.

### Lightning Bolt (6th Circle) ### {#lightning-bolt-spell}

__Uses:__ 1 prop, unlimited use - __Verbal:__ "Lightning Bolt" - __Material:__ 1
white boff arrow or javelin prop between 2'6" and 3'6" long - __Caveats:__
[OOC Calls](#ooc-calls){.new}

This spell allows the spellcaster to throw a stronger bolt of magic than [Magic
Missile](#magic-missile). The MC for the spell must be made following the
[Weapon Construction rules for Lightning Bolts](#lightning-bolt-construction)[.]{.new}

The prop is a physical representation of the magic. After it comes to rest, it
cannot be affected or moved by anyone other than the spellcaster, but it may
still be seen or guarded by anybody. The prop counts as a hand-and-a-half weapon
and must be thrown, not shot from a bow. The prop, including its shaft, strikes
as an [armor-piercing](#armor-piercing) [magic](#magic-call) blow to anything it
makes contact with, until it comes to rest. Once cast, it cannot be cast again
until the spellcaster recovers the prop. The prop is not considered a weapon and
does not cause [Spell Failure](#spell-failure), except while the spell is active
(i.e. from when the prop is thrown until it comes to rest).

### Magic Missile (4th Circle) ### {#magic-missile}

__Uses:__ Unlimited, while spellcaster has MC handy - __Verbal:__ "Magic
Missile" - __Material:__ 2 beanbags or foam & duct tape blocks, about 3"
diameter - __Caveats:__ [OOC Calls](#ooc-calls){.new}

When thrown, this spell strikes whatever it hits as if it were a magic sword. It
will damage every location it hits, until it comes to rest. The prop is a
physical representation of the magic. After it comes to rest, it cannot be
affected or moved by anyone other than the spellcaster, but it may still be seen
or guarded by anybody. A magic missile MC can be thrown with one hand. When a MC
is being thrown, the other hand may contain only a single magic missile MC or a
single-handed weapon or shield, and does not count toward dual-wielding for the
purposes of weapon restrictions. The prop is not considered a weapon and does
not cause [Spell Failure](#spell-failure) except while the spell is active (i.e.
from when the prop is thrown until it comes to rest). The spellcaster may only
throw their spell props, and may not pick up those thrown by another
spellcaster.

### Masterwork Hammer (6th Circle) ### {#masterwork-hammer}

__Uses:__ 1[/special]{.new} - __Verbal:__ 50 words - __Material:__ A boff hammer 
[within the spellcaster's weapon restriction]{.new} with "[Event-Stealable](#event-stealable)," "Masterwork Hammer," and the
spellcaster's name written on it - __Active:__ Special - __Caveats:__ [Enchanted
Items](#enchanted-items)

This spell creates a Masterwork Hammer which [the spellcaster may use]{.new} 
to repair non-armor, non-magic items (bows, weapons, shields) in 30 seconds. 
[The spellcaster may also use the hammer to]{.new} repair all armor on a target player
[by]{.new} using the hammer as the focus of the spell [for 60]{.new} seconds. While using
the hammer to make any type of repair, the spellcaster [cannot move their feet]{.new}
and should actively use the hammer to simulate repairing the target. If the
hammer is broken [or disenchanted]{.new}, the spellcaster may repair it by holding the item in both
hands [for 120 seconds]{.new}.

### [Mentor (1st Circle)]{.new} ### {#mentor}
[__Uses:__ 3 - __Verbal:__ 30 words]{.new}

[Allows the spellcaster to teach a legal spell from their spell mastery list.]{.new}

### Mystic Forge (4th Circle) ### {#mystic-forge}

__Uses:__ 1 - __Verbal:__ 25 words - __Material:__ 10-foot rope - __Active:__
Touch the rope - __Caveats:__ [Circles](#circles), [Suspension](#suspension)

This spell allows the spellcaster to create a Mystic Forge. The spellcaster may
name the circle with [Enchant Weapon](#enchant-weapon) or [Repair
Item](#repair-item), chosen at the time of casting. Until the Mystic Forge is
broken, the spellcaster need only stand in the circle, touch the target item,
and recite the named spell's VC to cast the spell. This does not use up any
castings of the named spell, and this can be done as many times as desired. No
one but the spellcaster may use the Mystic Forge in this manner.

### Pas (1st Circle) ### {#pas}

__Uses:__ 3 - __Verbal:__ "Pas, friend..." - __Material:__ Food, coin, or some
offering - __Active:__ Offer the MC to the target - __Caveats:__
[Compulsion](#compulsion)

This spell creates an uneasy, temporary truce between the target and the PC. To
cast this spell, the spellcaster offers something of value to the target and
says something along the lines of, "Pas, friend orc, and accept these shiny bits
to let me pass unharmed." If the target accepts the offering, they are magically
bound to not attack the spellcaster for 60 seconds, unless the target is
attacked. If the target is attacked or the spellcaster is slain, this spell ends
immediately. [Protect the Soul](#protect-the-soul) will block the effects of
this spell, as will [Resist Magic](#resist-magic).

### Precognition (3rd Circle) ### {#precognition}

__Uses:__ 3 - __Material:__ Divining paraphernalia (such as a crystal ball or
mirror)

This spell allows the spellcaster to gain non-specified information about the
plot from the EH or MM. How much information (if any) is at the discretion of
the EH or MM. One casting of Precognition may be pre-registered and the results
of this spell will be presented to the spellcaster at check-in for the event. If
a precognition is not given at the beginning of the event, the casting is not
used.

### Prophecy (6th Circle) ### {#prophecy}

__Uses:__ 1 - __Active:__ Ritual (Optional)

This spell allows the spellcaster to ask the EH or MM a question pertaining to
the plot of the event. The EH or MM will give the spellcaster as complete an
answer as they are willing. The method of delivering this knowledge is at the EH
or MM's discretion. A spellcaster may use drama, theatrics, or sacrifice during
the ritual to have a better chance of gaining information. After casting this
spell, the EH or MM may choose to release additional information to the
spellcaster at any time during the remainder of the event or until a spell
reset.

### Protect Item (1st Circle) ### {#protect-item}

__Uses:__ 3 - __Verbal:__ 20 words - __Material:__ Ribbon tied onto the item
protected. Remove the ribbon soon after the spell is expended/used to protect
the item - __Caveats:__ [Enchanted Items](#enchanted-items)[, [OOC Calls](#ooc-calls)]{.new}

This spell allows a single non-armor item to be protected from the next attack
that would normally damage it. For example, a protected sword struck by a
boulder would not be destroyed, but the wielder would still suffer normal damage
(e.g., death usually). The call for this spell is "Protect Item." A particular
item may only have one casting of Protect Item on it at a time. This spell does
not protect against [Disenchant](#disenchant).

### Protect the Soul (2nd Circle) ### {#protect-the-soul}

__Uses:__ 1 - __Verbal:__ 30 words and an explanation - __Material:__ Spell Sash
- __Caveats:__ [Enchanted Items](#enchanted-items), [[OOC Calls](#ooc-calls),]{.new}
[Spell Sash](#spell-sash)

This spell will protect the recipient from possession, spells under the
[compulsion](#compulsion) caveat, and similar effects as determined by the
MM/EH. When targeted by any spell or effect against which Protect the Soul
immunizes your PC, you must call "Protect the Soul!" The spell will last until
the sash is disenchanted or removed by the spellcaster. The spell will not
function if the recipient is soulless or their soul is not within their body for
any other reason.

### Protection from Boulder (1st Circle) ### {#protection-from-boulder}

__Uses:__ 2 - __Verbal:__ 20 words - __Material:__ Spell Sash - __Caveats:__
[Enchanted Items](#enchanted-items), [[OOC Calls](#ooc-calls),]{.new}
[Spell Sash](#spell-sash)

The spellcaster is protected from the next "boulder" call that strikes them.
This protection extends to all equipment they are carrying.

### Protection from Missile (2nd Circle) ### {#protection-from-missile}

__Uses:__ Unlimited, one at a time - __Verbal:__ 10 words - __Material:__ Spell
Sash - __Active:__ Kneel or lie on back, no weapons in hand - __Caveats:__
[Enchanted Items](#enchanted-items), [[OOC Calls](#ooc-calls),]{.new}
[Spell Sash](#spell-sash)

The recipient of this spell is protected from the next hit they take from an
arrow, javelin, or [Magic Missile](#magic-missile) spell. It is necessary to
call "Protection" when the spell activates. This spell will also protect
equipment (such as armor) that would otherwise be affected by the missile.

When the spell is cast, the recipient of the spell must be kneeling or be lying
on their back with no weapons in hand. This spell may be cast on a recipient
other than the spellcaster; to do so the spellcaster must have no weapons in
hand and touch the recipient while the spell is being cast. More than one
casting of this spell may be in effect on a single PC. If the spellcaster casts
this spell on another PC, they may not re-cast the spell until the sash is
returned to them.

### Purity to Disease (3rd Circle) ### {#purity-to-disease}

__Uses:__ 1, Self-only - __Verbal:__ 10 words - __Material:__ Spell Sash -
__Active:__ Lie on back, no weapons in hand - __Caveats:__  
[Enchanted Items](#enchanted-items), [[OOC Calls](#ooc-calls),]{.new}
[Spell Sash](#spell-sash)

Upon casting this spell, the spellcaster becomes completely immune to the
effects of diseases.

### Purity to Poison (3rd Circle) ### {#purity-to-poison}

__Uses:__ 1, Self-only - __Verbal:__ 10 words - __Material:__ Spell Sash -
__Active:__ Lie on back, no weapons in hand - __Caveats:__  
[Enchanted Items](#enchanted-items), [[OOC Calls](#ooc-calls),]{.new} 
[Spell Sash](#spell-sash)

Upon casting this spell, the spellcaster becomes completely immune to the
effects of poisons.

### Raise Dead (3rd Circle) ### {#raise-dead}

__Uses:__ 5 - __Verbal:__ 30 words - __Active:__ Spellcaster must be within 2
feet of corpse and there can be no weapons within 10 feet of the spellcaster -
__Caveats:__ [Spell Failure](#spell-failure)

This spell will raise a dead character, healing all of their injured limbs.
There can be no weapons within 10 feet of the spellcaster at any point while
casting this spell, or the spell will fail to work. For this purpose, a weapon
is considered to be anything with a legal striking surface - swords and arrows
are weapons, although bows are not. The player of the character being raised
must be present to represent the corpse. No proxy can be used for the corpse.

### Reforge (5th Circle) ### {#reforge}

__Uses:__ 2 - __Verbal:__ 30 words - __Material:__ Special - __Active:__ Special

This spell allows the spellcaster to reforge an existing item, either improving
it or repairing a magical item. The spellcaster may choose one of the following
options when casting the spell. Each option has its own effects and requirements
for casting the spell that must be met for the spell to be successfully cast.

Silver Weapon
: This option will allow the spellcaster to create a permanent silver weapon.
  This option has an additional MC of the weapon's weight in silver coins (plain
  aluminum roofing tins, aluminum "Coin of the Realm," etc.). The spellcaster
  must write the words "Silver" and "Stealable" on the blade of the weapon. The
  spellcaster should also write "Silvered by" and the spellcaster's name on the
  blade. All of the silver that is collected for the casting of this spell must
  be handed over to the EH. All silver weapons are stealable, and the
  spellcaster must explain to anyone having a weapon made silver that it will be
  stealable, and will be considered property of the Realms, to be passed back
  and forth within the game as a searchable item, for as long as it is silver. A
  silver weapon will lose the quality of being silver if the writing on the
  blade fades to the point of no longer being readable or if the weapon is OOC
  or IC broken (such as by a boulder). Players may not protect the writing in
  any way and may not rewrite it. A silvered weapon broken IC (i.e., by any
  means other than the fading of the writing or the physical destruction of the
  prop) can be repaired by an expenditure of this spell, without having to
  provide the necessary silver. Repairing a weapon in this way does not allow
  you to remake the prop or rewrite the word "Silver" on the weapon. Silver
  Weapon will not allow you to silver an existing magic item.

Reinforce Item
: This option will allow the spellcaster to make a normal weapon or shield
  unbreakable for the duration of the event. This option has an additional MC
  of a ribbon with the words "Reinforce Item" attached to the target item. The
  reinforced item will not be broken by attacks that would normally damage them
  (e.g. a boulder hitting it) as long as the spell remains active. If
  disenchanted through the spell [Disenchant](#disenchant), the ribbon must be
  removed and the effect will end, but the weapon will otherwise remain intact.
  This option may not be used on a magic item, silver weapon, or bows.

Restore Enchantment
: This spell allows the spellcaster to restore a currently-backed [Realms Magic
  Item](#magic-item-rules) that has been broken or disenchanted to working
  order. This option may have an additional AC of a required quest. Potions and
  other spell components are not subject to repair. If an item has been
  physically broken or made unsafe, then the item must be physically repaired or
  replaced with a near duplicate of the item. Note that patching or minor
  repairs may be acceptable instead of a full prop replacement depending on the
  situation. Upon informing the MM or EH of their intention to cast this spell,
  the EH or MM may either present to the spellcaster a quest for the completion
  of this spell or simply declare the item "repaired" at their discretion. The
  spell is wasted if the quest fails or if permission for a repair is outright
  denied.

### Regenerate the Soul (5th Circle) ### {#regenerate-the-soul}

__Uses:__ 1 - __Verbal:__ 30 words - __Material:__ An item similar to the
spellcaster's soul token, with some sort of indication that it is their second
soul token -__Caveats:__ [Regeneration](#regeneration-caveat), [Advanced
Regeneration](#advanced-regeneration)

This spell grants an advanced regeneration which returns a character to life
from soullessness only. After regenerating in this fashion, the material
component becomes the spellcaster's new soul token.


### Regeneration (5th Circle) ### {#regeneration-spell}

__Uses:__ 2 - __Verbal:__ 30 words - __Active:__ Spellcaster must sit on the
ground with weapons a minimum of 10 feet away while casting this spell -
__Caveats:__ [Regeneration](#regeneration-caveat), [Advanced
Regeneration](#advanced-regeneration), [Spell Failure](#spell-failure)

This spell grants the spellcaster an [advanced
regeneration](#advanced-regeneration). If you have learned this spell twice, you
may cast a [Raise Dead](#raise-dead) to "recharge" one usage. These recharges
cannot come from [Circle of Healing](#circle-of-healing), or any other raising
effects other than the [Raise Dead](#raise-dead) spell.

### Repair Armor (1st Circle) ### {#repair-armor}

__Uses:__ 5 - __Material:__ Disposable or focus - __Active:__ Hold armor and MC
for 15 second count

This spell will repair one hit location of armor. The AC should simulate
physically repairing the armor, such as tapping it with a focus, like a
boff-hammer.

### Repair Item (2nd Circle) ### {#repair-item}

__Uses:__ 5 - __Verbal:__ 20 words - __Active:__ Touch the target item with both
hands

This spell repairs any one normal object[: a weapon, shield, bow or armor.]{.new} 
It cannot [repair]{.new} an item with a special property, such as a 
[magic item](#magic-item-rules). [If used to repair armor that is being worn, 
a single casting will repair all pieces of armor that can be legally called by 
the wearer.]{.new} The spellcaster may have nothing else in their hands while 
casting this spell.

### Resist Death (6th Circle) ### {#resist-death}

__Uses:__ Unlimited, one at a time - __Verbal:__ 30 words - __Material:__ Spell
Sash - __Active:__ Kneel or lie on back, no weapons in hand. - __Caveats:__
[Enchanted Items](#enchanted-items), [[OOC Calls](#ooc-calls),]{.new}
[Spell Sash](#spell-sash)

The spellcaster is protected from any damaging attack for 1 hit. The call for
this is "Resist Death." The spellcaster can choose when to utilize this effect.

### Resist Magic (5th Circle) ### {#resist-magic}

__Uses:__ 3 - __Verbal:__ 20 words [- __Caveats:__ [OOC Calls](#ooc-calls)]{.new}

This spell prepares a burst of null-magic within the spellcaster. If the
spellcaster so desires, they may ignore a single magical effect. This ability
can be used at any time, whether the spellcaster is dead or not. A spellcaster
may not be under the effect of more than one Resist Magic spell at the same
time. When targeted by a spell or effect against which Resist Magic protects
them and the spellcaster wishes to ignore the effect, they call "Resist Magic."
[For example:]{.new} this spell will allow the spellcaster to treat a blow from a magic weapon as if
it were a normal weapon blow, ignore the effect of any spell when it is first
cast, [ignore any potion when it's first applied,]{.new} or cross the boundary of a [Circle of Protection](#circle-of-protection).
The spell ends if the spell [Disenchant](#disenchant) is cast upon the
spellcaster (although the spellcaster can use the Resist Magic to prevent the
[Disenchant](#disenchant) from removing any other spells upon them). This spell
cannot be cast on anyone other than the spellcaster and will only protect the
spellcaster, not anything they have or possess.

### Ritual of Banishment (6th Circle) ### {#ritual-of-banishment}

__Uses:__ Special, see below - __Verbal:__ 40 words in "burst" form, otherwise
special, see below

This spell allows the spellcaster to use their knowledge of magic and the planes
to shift a creature back to its home dimension or to scatter the magic of a
being. The spellcaster has two ways of performing this ritual. If not given time
to prepare a stronger ritual, the spellcaster may simply use this spell in a
"burst" fashion. To do so, the spellcaster must get the attention of the
creature and say the 40 words, which must begin with "I banish you to the place
from which you came..." During the casting of the spell, the spellcaster is
still open to any retaliation from the creature being banished.

If given more time, the spellcaster can craft a more potent spell to send a
creature to its home plane or strip the being of magics, potentially weakening
it in the process. The player may do so by performing a ritual in front of the
MM or EH. By reading the available magic and essences of the creature, this
ritual may grant the spellcaster knowledge and further steps that can be taken
to banish or weaken the creature, possibly leading to the final incantation.
After the initial casting of the ritual, this spell is unleashed when the
spellcaster uses the researched magic upon the creature. The prepared spell may
only be good for one attempt, whether successful or not. Greater success in
crafting this magic may be achieved through knowledge of the True Name of the
creature, a physical or magical part of it or through a particularly potent
ritual.

Players who use drama and theatrics in their ritual are more likely of achieving
better results. This spell will only function upon NPCs. Once this spell is
attempted upon a creature, either in short or long formats, the spellcaster may
not cast this spell again for an hour. You are encouraged to use a small
timepiece to keep track of this time. Creatures that are shown to be too
powerful from the preparation ritual do not weaken the spellcaster. You may not
begin a preparation ritual on the same day as an uncompleted previous spell.
Upon completion of a more in-depth ritual, the spellcaster should ask the EH or
MM how long this spell is exhausted for, which may be the rest of the day. If
you know this spell, you must inform the EH or MM at check-in.

### Séance (4th Circle) ### {#seance}

__Uses:__ 1 - __Verbal:__ 20 words to start - __Material:__ 3-minute
hourglass/timer.

This spell allows the spellcaster to have an extended discussion with a spirit,
either one of another world or of a soulless character. Upon informing the EH or
MM of their intent to cast this spell, the spellcaster must start the ritual by
flipping the hourglass. If the spirit does not arrive within the first three
minutes, then the casting is not used. If the spirit arrives, let the glass run
out and flip it again. The spellcaster and spirit may then speak freely until
all the sands have fallen. If the spirit stays longer than three minutes, the
spellcaster may continue to converse with it. Please note that this spell does
not change any behavior on the part of the spirit, and it may choose not to
talk.

Whether a soulless character can answer is entirely up to the discretion of the
EH or MM, who must be present for the ritual. This spell in no way grants the
knowledge of the circumstances of a soulless character's death. If they are
allowed to be contacted, the soulless PC can still refuse to answer, is not
compelled to speak, can lie or tell the truth freely, and can end the séance at
any time. A PC contacted with a séance must leave after three minutes.

### Second Chance (6th Circle) ### {#second-chance}

__Uses:__ 1 - __Verbal:__ 30 words - __Material:__ A token with the
spellcaster's name

This spell allows the spellcaster to recover once, even from the most grievous
of wounds and situations.

Upon casting this spell, the spellcaster must give their token to the MM. At any
time afterwards, the spellcaster may activate the spell, which removes them from
play. They must then go find the MM. All stealable items in possession of the
spellcaster must be left behind. This spell may be activated even if the
spellcaster is dead or soulless.

The MM will then place them somewhere on site (location determined by the MM)
and return the token. Upon being placed, the spellcaster is alive and unwounded,
and is given back their soul token if they were soulless.

### Seed of Life (6th Circle) ### {#seed-of-life}

__Uses:__ Unlimited, while spellcaster has MC handy - __Verbal:__ 30 words and
an explanation - __Material:__ 2 tokens with the spellcaster's name and the
words "Seed of Life" on it - __Caveats:__ [Regeneration](#regeneration-caveat),
[Basic Regeneration](#basic-regeneration)

When cast on a dead body, this spell grants the recipient a basic regeneration.
The spellcaster must hand the MC to the recipient when the spell is cast.

Once the spell ends, the recipient should return the MC to the spellcaster as
soon as reasonably possible. Other than this, the MC is neither stealable nor
transferable in any way. If the recipient is rendered soulless, returned to
life, or raised as undead, the spell ends. If the recipient is diseased, this
spell will also cure them of their disease upon completion of the spell
(although the regeneration time will still be doubled from the effects of the
disease). This spell has no effect on the undead.

[If the spell is cast with both tokens on the same recipient, then the recipient 
will regenerate in three-fourths the normal time (usually 90 seconds).]{.new}

### Shapeshifting (4th Circle) ### {#shapeshifting}

__Uses:__ 2 - __Material:__ Makeup and/or mask and any disguise garb -
__Active:__ Change into disguise - __Caveats:__ [Enchanted
Items](#enchanted-items)

This allows the spellcaster to shapeshift into a humanoid monster of about their
height and size. This transformation takes as long to complete as it takes the
player to change into the appropriate disguise outfit. The type and features of
the monster are up to the player. Once the shapeshifting is complete, the player
will respond to the spell [Identify](#identify) as the new type of monster. This
spell will mimic a general monster type, and cannot accurately impersonate a
named or unique monster, or appear to be another PC. You are free however, to
attempt to convince your victims that you are more important than you actually
are.

The shapeshifted form confers no combat benefit or other NPC power, though they
can appear to wear armor or carry larger weapons to complete the disguise. You
may in no way signal to NPCs that you are NPCing. The shapeshifted form ends if
you are killed or if any part of your disguise is [disenchanted](#disenchant).

In addition, at the door of the event, the player is allowed to ask the EH or MM
to borrow an appropriate mask for the event in order to complete the illusion.
There is no guarantee that they will be able to provide the materials, so you
should bring your own.

### Skew Divination (3rd Circle) ### {#skew-divination}

__Uses:__ 2 - __Verbal:__ 30 words - __Material:__ Scroll with a name of an
item, person, group, place, or situation - __Active:__ Give scroll to MM

This spell will alter the next [Guidance](#guidance), [Fortune
Tell](#fortune-tell), [Precognition](#precognition), [Find the
Path](#find-the-path), [Foretell](#foretell), [Séance](#seance),
[Vision](#vision), or [Prophecy](#prophecy) spell cast about the target at that
event, giving them misinformation. How much the spell is altered is up to the
MM. To cast this spell, the spellcaster must write the name of the target (item,
person, group, place, or situation) on a scroll, sign the scroll, and give the
scroll to the MM.

### Soul Bane (3rd Circle) ### {#soul-bane}

__Uses:__ 1 - __Active:__ Destroy a dead body. Following the final body
destroying blow the spellcaster says "Soul Bane." [- __Caveats:__ 
[OOC Calls](#ooc-calls)]{.new}

This spell alters the next [Call the Soul](#call-the-soul) cast on the target by
reversing which object is successful and unsuccessful. The spellcaster must
inform the MM whose body they destroyed and cast Soul Bane on as soon possible.
The effect triggers the next time the target's soul is called. The spell ends
after the first [Call the Soul](#call-the-soul), whether it was successful or
not. Only one Soul Bane can be active on a person at a time.

### Speak (1st Circle) ### {#speak}

__Uses:__ 2 - __Verbal:__ "Speak, friend... " - __Material:__ An offering for
the creature to be spoken with - __Active:__ The spellcaster approaches the
creature with no weapons and with an offering in plain sight, and hands it to
the monster

This spell allows the spellcaster to approach a creature and present an offering
to them. If the offering is taken, the creature now has the ability to speak and
understand the language of the spellcaster. This ability lasts until the
creature is no longer in possession of the offering. No creature approached has
to take the offering, nor is there any guarantee that the creature will speak to
you.

### Speak with Dead (1st Circle) ### {#speak-with-dead}

__Uses:__ 10 - __Verbal:__ An explanation, followed by a question - __Caveats:__
[Compulsion](#compulsion)

This spell allows the spellcaster to ask a corpse one "yes or no" question. The
corpse may only answer "Yes," "No," or "Abstain," and it may not lie. An
abstention means that the spirit cannot or does not want to answer the question.
Before asking the questions, the spellcaster must explain to the corpse's player
what the acceptable responses are and that the character may not lie.

### Strange Brew (1st Circle) ### {#strange-brew}

__Uses:__ Special, see [Alchemy](#alchemy) - __Caveats:__
[[OOC Calls](#ooc-calls),]{.new} [Potions](#potions-caveat)

You may create 2 additional types of potions listed below using
[Alchemy](#alchemy). Upon learning Strange Brew, you must record the potions you
are choosing to learn, and their specific rules, in your spellbook:

* (1 Point) [Potion of Cure Disease]{#potion-of-cure-disease}: Cures the target
  of all disease.
* (1 Point) [Potion of Heal Limb]{#potion-of-heal-limb}: Heals all damaged limbs
  of a living recipient
* (2 Points) [Potion of Animate Lesser Undead]{#potion-of-animate-lesser-undead}: 
  Animates a corpse as per spell the [Animate Lesser
  Undead](#animate-lesser-undead). The spellcaster must also provide the
  necessary material requirements of that spell for when the potion is
  administered. Treat the person who applies this potion as the controller of
  the undead.
* (3 Points) [Potion of Disenchant]{#potion-of-disenchant}: Disenchants an
  enchanted or magic item, as per the spell, [Disenchant](#disenchant).
* (4 Points) [Potion of Combat Raise Dead]{#potion-of-combat-raise-dead}: Raises
  a dead character, healing all of their injured limbs.
* (5 Points) [Potion of Acid]{#potion-of-acid}: Deals 200 blows to a body, only
  usable on non-living objects.
* (5 Points) [Potion of Animate Undead]{#potion-of-animate-undead}: Animates a
  corpse as per the spell [Animate Undead](#animate-undead). The spellcaster
  must also provide the necessary material requirements of that spell for when
  the potion is administered. Treat the person who applies this potion as the
  controller of the undead.
* (5 Points) [Potion of Soul Snare]{#potion-of-soul-snare}: Recipient's soul is
  returned to them as if the spell [Call the Soul](#call-the-soul) was
  successfully cast on them and their soul was called. The MM must be present
  for this potion to be used.
* [(8 points) [Potion of Séance]{#potion-of-seance}: The recipient can cast a 
  usage of [Séance](#seance) as per the spell. If the [Séance](#seance) fails, 
  the potion is used up.]{.new}  

This does not grant any points to alchemy.

### Transformation (6th Circle) ### {#transformation}

__Uses:__ 2 - __Material:__ A pair of Transformation Claws - __Caveats:__
[Regeneration](#regeneration-caveat), [Advanced
Regeneration](#advanced-regeneration), [[OOC Calls](#ooc-calls),]{.new}
[Suspension](#suspension)

The spellcaster releases their inner nature. The effect is a transformation.
Each individual spellcaster may have a different form, but that form is
consistent to the spellcaster (i.e., Matt's altered form is different than
Sally's, but Matt's altered form is always the same any time he shifts to it).
The spell must be unlearned to alter that form.

A complete description of the altered form must be in the spell description. The
spellcaster must alter their appearance when in the transformed state. They must
wear a different tabard, makeup, prosthetics, mask, or some other major
signifying indicator that they are "not quite right." Details should also be
listed in the spellcaster's spellbook.

Any potions or other spells with a lingering effect cast by the spellcaster are
suspended when they transform. The suspension ends when the spellcaster reverts
to their common form.

All altered forms have the following advantages/disadvantages:

* Has the use of florentine claws, 18" maximum. Cannot use any other weapons.
  Claws must be of matched length. Cannot use or manipulate anything except with
  those claws (after all, they're the end of their hands). These claws do not
  break the spellcaster's weapon length restriction.
* The claws are physical representations for the spell, and are not really there
  until the spell is cast. They cannot be used by anyone other than the
  spellcaster, and they may only use them when under the effects of the spell.
  The claws must be clearly labeled with the phrase "Transformation Claws" and
  the spellcaster's name. The claws are not considered weapons for the purpose
  of Spell Failure. Furthermore, they are unbreakable and immune to the effect
  of a boulder, though the spellcaster is killed as normal if struck in the
  claws.
* Cannot cast any spells while in altered form, except those granted as part of
  the Transformation.
* The spellcaster may revert back to their common form, ending the spell at any
  time.

A spellcaster has 7 points to construct their altered form:

Armor Options:

* _Natural Armor:_ 1 point. Grants light armor on all hit locations. Can only be
  purchased once. Cannot be used in conjunction with Regenerating Armor.
* _Regenerating Armor:_ 3 points. Grants light armor on all hit locations, which
  can be regenerated. Can only be purchased once. To activate the regenerating
  effect, the spellcaster must lie on their back without attacking. Once so
  positioned, the spellcaster must remain still for 10 seconds per damaged hit
  location (repairing every hit location would take 70 seconds). If interrupted
  before all damaged hit locations are repaired, the effect fails with no armor
  points regenerated. Cannot be used in conjunction with Natural Armor.

Both armor options may be repaired by casting [Repair Item](#repair-item).

Weapon Upgrades:

* 1 point for 2' claws.
* 2 points for 2'6" claws.
* 3 points for 3' claws.
* 4 points for 3'6" claws.
* _Poison Weapons:_ 4 points. The spellcaster may call "[Poison](#poison)" each
  time they swing their claws.
* _Armor-Piercing Weapons:_ 5 points. The spellcaster may call
  "[Armor-Piercing](#armor-piercing)" each time they swing their claws.
* _Axe, Hammer, or Mace claws:_ 1 point. The spellcaster may call "Axe,"
  "Hammer," or "Mace" each time they swing their claws. The specific call must
  be chosen when the form is initially created. This option may only be taken
  once. The claws should reflect the nature of the call chosen.

Miscellaneous Abilities:

* _Limited Regeneration:_ 2 points for one use. All of the spellcaster's limbs
  regenerate after 30 seconds, as an unlimited effect. Cannot be in combat while
  healing limbs in this fashion. The spellcaster has a single [advanced
  regeneration](#advanced-regeneration). 1 point for each additional extra life
  beyond the first.
* _[Death Watch](#death-watch):_ 2 points. As per the spell.
* _[Immunity to Poison](#immunity-to-poison):_ 1 point. May only cast on self,
  otherwise as per the spell.
* _[Cure Disease](#cure-disease):_ 1 point. May only cast on self, otherwise as
  per the spell.
* _[Heartiness](#heartiness):_ 1 point. As per the spell.

Spells gained through transformation are as per the spell specified, including
number of castings. Each time you transform, the uses reset. Spells gained this
way may only be cast while transformed.

### Transmute Self (4th Circle) ### {#transmute-self}

__Uses:__ 3 - __Verbal:__ 10 word chant, repeated. The verbal must be chanted
loudly and clearly - __Caveats:__ [Chanting](#chanting), [[OOC Calls](#ooc-calls),]{.new}
[Suspension](#suspension)

This spell provides an immense amount of protection to the spellcaster, but also
requires an immense amount of concentration. This spell only takes effect once
the spellcaster has completed the VC once. While transmuted, the spellcaster is
completely immune to all forms of damage, magical or otherwise, regardless of
whether the material into which the spellcaster transmutes is vulnerable to any
form of damage. It does not make the spellcaster invisible or undetectable. The
spellcaster must choose what they are capable of attuning to when learning the
spell. Choices are: trees, stone, or earth. To transmute, the spellcaster must
embrace or lie down on the object they are capable of attuning to (so those who
can attune to trees hug a tree, to stone lie on or hug a rock, or to earth lie
on the ground). While transmuted, the spellcaster is "stuck" and cannot be
dragged. The object the spellcaster attunes with MUST be at least as massive as
the spellcaster. The spellcaster must keep their eyes closed and remain
perfectly still, and they must be constantly chanting their verbal while
transmuted. The spellcaster must chant loudly and clearly. If anything
interrupts the spellcaster's concentration, the spell is broken. [OOC explanations 
(such as combat calls) do not interrupt this spell. For example, if a PC chanting 
a Transmute Self spell and is hit by a weapon, they may call "No effect" without 
interrupting the spell.]{.new} As soon as the spellcaster moves, opens their eyes, 
or stops chanting, the spell ends. The spellcaster may not transmute for at least 
one slow 200 second count after regaining their proper form. The spellcaster should 
use their common sense when deciding where to transmute. Pick a safe location, not 
the middle of a trail or a high combat area.

### Vision (5th Circle) ### {#vision}

__Uses:__ 1

This spell allows the spellcaster to ask the EH or MM a question. The EH will
then reveal to the spellcaster as complete a description as they are willing to
give, giving them a vision relating to it.

### Ward: Enchanted Beings (5th Circle) ### {#ward-enchanted-beings}

__Uses:__ Unlimited - __Verbal:__ 20 words, repeated continuously, stating
purpose of spell - __Material:__ Focus - __Caveats:__ [Chanting](#chanting),
[Suspension](#suspension), [Wards](#wards)

This spell prevents [Enchanted Beings](#enchanted-beings) from attacking the
spellcaster while it is active.

### Ward: Undead (2nd Circle) ### {#ward-undead}

__Uses:__ Unlimited - __Verbal:__ 10 words, repeated continuously, stating
purpose of spell - __Material:__ Focus - __Caveats:__ [Chanting](#chanting),
[Suspension](#suspension), [Wards](#wards)

This spell prevents [Undead](#undead) from attacking the spellcaster while it is
active.

### Zombie Walk (1st Circle) ### {#zombie-walk}

__Uses:__ Unlimited, 1 at a time - __Verbal:__ 10 words, plus an explanation -
__Active:__ Touch the target - __Caveats:__ [Undead](#undead), [Walking
Dead](#walking-dead)

This spell allows the spellcaster to animate a corpse, making it follow them for
as long as they concentrate on the spell. If the spellcaster engages in combat
either by attacking or by being struck, the spell ends and the corpse falls to
the ground. In order to cast this spell, the spellcaster must recite the verbal
and give the player of the corpse a brief explanation of what they should do,
making sure they know when to fall down.