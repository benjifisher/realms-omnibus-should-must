# [9: Realms Administrative Positions]{.new} # {#realms-administrative-positions}

## [9.1 List of Positions]{.new} ## {#list-of-positions}

The administrative positions elected each year are:

* [Administrative Meeting Organizers](#meeting-organizers){.new}
* [Arbitration Committee](#arbitration-committee){.new}
* [Death Marshal](#death-marshal){.new}
* [Event List Administrator](#event-list-administer){.new}
* [Minister of Magic Items](#minister-of-magic-items){.new}
* [Omnibus Editorial Committee](#omnibus-editorial-committee){.new}

## [9.2 Position Descriptions]{.new} ## {#position-descriptions}

### [Administrative Meeting Organizers]{.new} ### {#meeting-organizers}

[The Administrative Meeting Organizers are responsible for organizing,
announcing, and running both the Players' Meeting and the Event Holders'
Council for the following calendar year.]{.new}

[The current Administrative Meeting Organizer is Pi Fisher
(<nerd.of.pi@gmail.com>, 617-615-6373).]{.new}

### Arbitration Committee ### {#arbitration-committee}

The [Arbitration Committee](https://www.realmsnet.net/arbitration) is a group of
five members and two alternates nominated and elected yearly at the EHC that
seeks to resolve issues by speaking or meeting with the affected individuals
and, in the case where actions are detrimental to the community, levy penalties
against individuals. The Arbitration Committee can be reached at
<arbitration@lists.realmsnet.net>.

[The Arbitration Committee should strive to bring issues to resolution within 30 
days of a complaint being filed. If they are unable to do so within 45, days they
must notify the Event Holders mailing list of the charges and the reason why they've
been unable to come to a decision.]{.new}

When individuals are found responsible for breaking rules by a simple majority
of the Arbitration Committee, the committee may levy punishment consistent
with both the charge and a goal of working to resolve the issue in the future.
[Additional information regarding the scope and processes of the AC can be found
online at
[https://www.realmsnet.net/arbitration](https://www.realmsnet.net/arbitration).]{.new}

[Any party involved in a case decided by the Arbitration Committee may request that 
the designated organizer of the next EHC consider convening an emergency meeting. A]{.new}
2/3 vote of the [EHC]{.new} can overturn or amend the actions of the Arbitration Committee.

Every year, at the EHC, the Arbitration Committee must present a report which
details:

* The total number of items submitted in the past year
* The number of non-actionable reports of behavior submitted in the past
  year
* The number of complaint forms submitted in the past year
* The number of complaints that were actionable
* A brief summary of each actionable complaint, including its conclusion

The report should be made publicly available on
[RealmsNet](https://www.realmsnet.net/arbitration).

The players elected to the Arbitration Committee are [Angela Gray]{.new}, Dave Hayden, 
[Cal Marsden]{.new}, [Steve Nelson.]{.new} and [Jason Rosa]{.new}. 
The two alternates are [Tom Gallagher]{.new} and [Nataliya Kostenko]{.new}.

The Arbitration Committee can be reached at
<arbitration@lists.realmsnet.net>

### [Death Marshal]{.new} ### {#death-marshal}

[The Death Marshal is responsible for tracking and maintaining ticks that are
reported by EHs from their event for a character, or reported by a character's
player for their own character.]{.new}

[The Current Death Marshal is Ian Pushee (<bright@giantsquid.net>).]{.new}

### [Event List Administrator]{.new} ### {#event-list-administer}

[The Event List Administrator is responsible for maintaining an official
list of events. The list is available online at <http://www.realmsnet.net>.]{.new}

[The current Event List Administrator is Ian Pushee (<bright@giantsquid.net>).]{.new}

### [Minister of Magic Items]{.new} ### {#minister-of-magic-items}

[The Minister of Magic Items is responsible for maintaining an
official list of [Realms Magic Items](#magic-item-rules). The list is available
to EHs online at <http://www.realmsnet.net>.]{.new}

[The current Minister of Magic Items is Ian Pushee (<bright@giantsquid.net>).]{.new}

### Omnibus Editorial Committee ### {#omnibus-editorial-committee}

Each year, the Event Holders' Council nominates and elects a five-person
Omnibus committee to format and maintain a master copy of the rulebook. During
the editing process, the committee has the power to make the following minor
changes to the rulebook:

* Spelling errors
* Grammar errors, such as punctuation, pluralization, etc.
* Spells mentioned that no longer exist in the Omnibus
* Spells that have had their names changed
* Text that was intended to be altered, added, or removed as part of a proposal
  voted in by the EHC, but was omitted from the proposal text and therefore
  creates an inconsistency within the Omnibus. These edits cannot
  substantially change the impact of the proposal
* Rearranging the orders of tables and lists, as long as these rearrangements
  don't change how any rules work

Changes made to the document must be made by unanimous decision by the
committee, who must notify the Event Holders' List of changes not specifically
approved. All copies of the Omnibus produced for public use must be derived
from the master text, which is published on RealmsNET, although formatting and
printing process are at the discretion of the individual publisher. A digital
copy of the Omnibus text will be provided to any member of the community on
request.

Editorial changes as outlined above may continue to be made throughout the year
after the master text is published. The Omnibus Editorial Committee is
responsible for submitting changes to the Event Holders' List. If no
objections are made, the change is automatically accepted and the master text
will be updated. If any objections are made, the proposed change must be
submitted through the official proposal submission process outlined in the
Omnibus and voted upon at the following year's Event Holders' Council. The
Omnibus Editorial Committee must keep a log of all changes made, which must be
made available to the Event Holders upon request.

The Editorial Committee for the [2020]{.new} Omnibus is Pat Bobell, Alex Cannamela, 
Pi Fisher, [Justin Mitchell]{.new}, and [Emily Murphy]{.new}.